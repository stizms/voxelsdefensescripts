﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using System;

public static class Ext
{
    public static List<string> GetFieldDesc(this Type t)
    {
        List<string> li = new List<string>();
        var arr = t.GetProperties();

        foreach (var val in arr)
        {
            var ar = (DescriptionAttribute[])val.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (ar != null && ar.Length > 0)
            {
                li.Add(ar[0].Description);
            }
        }

        return li;
    }

    // enum
    public static string ToDesc(this System.Enum a_eEnumVal)
    {
        var da = (DescriptionAttribute[])(a_eEnumVal.GetType().GetField(a_eEnumVal.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
        return da.Length > 0 ? da[0].Description : a_eEnumVal.ToString();
    }

    public static NormalTowerData GetNormalTowerValue(this Tower towerName)
    {
        return TableManager.Ins.normalTowerTb[towerName];
    }

    public static MagicTowerData GetMagicTowerValue(this Tower towerName)
    {
        return TableManager.Ins.magicTowerTb[towerName];
    }

    public static RareTowerData GetRareTowerValue(this Tower towerName)
    {
        return TableManager.Ins.rareTowerTb[towerName];
    }

    public static UniqueTowerData GetUniqueTowerValue(this Tower towerName)
    {
        return TableManager.Ins.uniqueTowerTb[towerName];
    }

    public static EpicTowerData GetEpicTowerValue(this Tower towerName)
    {
        return TableManager.Ins.epicTowerTb[towerName];
    }
    public static TowerUpgradeData GetNeedCardValue(this int towerLevel)
    {
        return TableManager.Ins.towerUpgradeTb[towerLevel];
    }

    public static StageData GetStageEnemyValue(this int nowStage)
    {
        return TableManager.Ins.stageTb[nowStage];
    }

    public static Item_BoxData GetItemBoxValue(this Box itemName)
    {
        return TableManager.Ins.item_BoxTb[itemName];
    }

    public static Item_CrystalData GetItemCrystalValue(this Crystal itemName)
    {
        return TableManager.Ins.item_CrystalTb[itemName];
    }

    public static Item_LifeData GetItemLifeValue(this Life itemName)
    {
        return TableManager.Ins.item_LifeTb[itemName];
    }

    public static Item_CoinData GetItemCoinValue(this Coin itemName)
    {
        return TableManager.Ins.item_CoinTb[itemName];
    }
}

public enum Rank
{
    [Description("SRank")]
    S,
    [Description("ARank")]
    A,
    [Description("BRank")]
    B,
    [Description("CRank")]
    C,
}

public enum eTable
{
    [Description("-")]
    None = -1,

    [Description("0")]
    Config,

    [Description("153920418")]
    NormalTower,

    [Description("57760704")]
    MagicTower,

    [Description("261349371")]
    RareTower,

    [Description("1122686916")]
    UniqueTower,

    [Description("1602106447")]
    EpicTower,

    [Description("179031061")]
    TowerUpgrade,

    [Description("727304028")]
    Stage,

    [Description("1384850670")]
    Item_Box,

    [Description("1338150214")]
    Item_Crystal,

    [Description("185127981")]
    Item_Life,

    [Description("141377745")]
    Item_Coin,

    Max,

    [Description("923162867")]
    Login,

    [Description("272871317")]
    Ranking,
}

public enum TowerType
{
    [Description("None")]
    None = -1,
    [Description("Normal")]
    Normal,
    [Description("Magic")]
    Magic,
    [Description("Rare")]
    Rare,
    [Description("Unique")]
    Unique,
    [Description("Epic")]
    Epic
}

public enum AttackType
{
    [Description("Melee")]
    Melee,
    [Description("LongRange")]
    LongRange,
    [Description("WideAttack")]
    WideAttack
}

public enum Tower
{
    None = -1,
    
    [Description("BusinessMan")]
    BusinessMan,
    [Description("Farmer")]
    Farmer,
    [Description("FemaleHero")]
    FemaleHero,
    [Description("FemaleTrenchCoat")]
    FemaleTrenchCoat,
    [Description("InjuredSurvivor")]
    InjuredSurvivor, // 여기까지 C
    [Description("MaleSurvivor01")]
    MaleSurvivor01,
    [Description("MaleSurvivor02")]
    MaleSurvivor02,
    [Description("MaleSurvivor03")]
    MaleSurvivor03,
    [Description("MaleSurvivor04")]
    MaleSurvivor04,
    [Description("Prisoner")]
    Prisoner, // 여기까지 B
    [Description("FemalePyro")]
    FemalePyro,
    [Description("FemaleSoldier")]
    FemaleSoldier,
    [Description("HoodedMan")]
    HoodedMan,
    [Description("Hunter")]
    Hunter,
    [Description("OldMale")]
    OldMale,
    [Description("RoadWorker")]
    RoadWorker,
    [Description("Scout")]
    Scout,
    [Description("Sheriff")]
    Sheriff, // 여기까지 A
    [Description("BioHazard")]
    BioHazard,
    [Description("Doctor")]
    Doctor,
    [Description("MaleTrenchCoat")]
    MaleTrenchCoat,
    [Description("Hazard")]
    Hazard, // 여기까지 S

    /* 타워 종류 갯수 22*/
    TowerCategoryNumber,
}

public enum Enemy
{
    None = -1,

    [Description("Z_AirPortSecurity")]
    Z_AirPortSecurity,
    [Description("Z_AirPortWorker")]
    Z_AirPortWorker,
    [Description("Z_Bellhop")]
    Z_Bellhop,
    [Description("Z_BusinessMan")]
    Z_BusinessMan,
    [Description("Z_Cheerleader")]
    Z_Cheerleader,
    [Description("Z_Clown")]
    Z_Clown,
    [Description("Z_Farmer")]
    Z_Farmer,
    [Description("Z_FarmersDaughter")]
    Z_FarmersDaughter,
    [Description("Z_Firefighter")]
    Z_Firefighter,
    [Description("Z_FootballPlayer")]
    Z_FootballPlayer,
    [Description("Z_GrandMa")]
    Z_GrandMa,
    [Description("Z_Hobo")]
    Z_Hobo,
    [Description("Z_Industrial")]
    Z_Industrial,
    [Description("Z_Mechanic")]
    Z_Mechanic,
    [Description("Z_Mountie")]
    Z_Mountie,
    [Description("Z_Pilot")]
    Z_Pilot,
    [Description("Z_Pimp")]
    Z_Pimp,
    [Description("Z_Prisoner")]
    Z_Prisoner,
    [Description("Z_Prostitute")]
    Z_Prostitute,
    [Description("Z_RoadWorker")]
    Z_RoadWorker,
    [Description("Z_Robber")]
    Z_Robber,
    [Description("Z_Runner")]
    Z_Runner,
    [Description("Z_Santa")]
    Z_Santa,
    [Description("Z_Shopkeeper")]
    Z_Shopkeeper,
    [Description("Z_Soldier")]
    Z_Soldier,
    [Description("Z_StreetMan")]
    Z_StreetMan,
    [Description("Z_Tourist")]
    Z_Tourist,
    [Description("Z_Trucker")]
    Z_Trucker,

    EnemyCategoryNumber,
}

public enum Store
{
    None = -1,

    [Description("Box")]
    Box,
    [Description("Life")]
    Life,
    [Description("Crystal")]
    Crystal,
    [Description("Coin")]
    Coin,

    ItemCategoryNumber,
}

public enum Box
{
    None = -1,

    [Description("Box1")]
    Box1,

    [Description("Box2")]
    Box2,

    [Description("Box3")]
    Box3,

    [Description("Box4")]
    Box4,

    BoxCategoryNumber,
}

public enum Crystal
{
    None = -1,

    [Description("Crystal1")]
    Crystal1,

    [Description("Crystal2")]
    Crystal2,

    [Description("Crystal3")]
    Crystal3,

    [Description("Crystal4")]
    Crystal4,

    CrystalCategoryNumber,
}

public enum Life
{
    None = -1,

    [Description("Life1")]
    Life1,

    [Description("Life2")]
    Life2,

    [Description("Life3")]
    Life3,

    [Description("Life4")]
    Life4,

    LifeCategoryNumber,
}

public enum Coin
{
    None = -1,

    [Description("Coin1")]
    Coin1,

    [Description("Coin2")]
    Coin2,

    [Description("Coin3")]
    Coin3,

    [Description("Coin4")]
    Coin4,

    CoinCategoryNumber,
}

public enum PayCategory
{
    None = -1,

    Coin,
    Cash,
    Crystal
}

public interface IColor
{
    Color color { get; set; }
}


public enum State
{
    IDLE,
    ATTACK,
}

public enum Map
{
    Forest,
    Ice
}

public enum SoundCategory
{
    None = -1,

    [Description("LoadingBGM")]
    LoadingBGM,
    [Description("MeleeAttack")]
    MeleeAttack,
    [Description("MainBGM")]
    MainBGM,
    [Description("LongRangeAttack")]
    LongRangeAttack,
    [Description("PopUpSound")]
    PopUpSound,
    [Description("WideAttack")]
    WideAttack,
    [Description("ExitSound")]
    ExitSound,
    [Description("UpgradeSound")]
    UpgradeSound,
    [Description("StartSound")]
    StartSound,
    [Description("BoxOpenSound")]
    BoxOpenSound,
    [Description("BuySound")]
    BuySound,
    [Description("BuildSound")]
    BuildSound,
    [Description("FusionSound")]
    FusionSound,
    [Description("SellSound")]
    SellSound,
    [Description("GameOverSound")]
    GameOverSound,
}

public static class Define
{
    public const float MELEE_ANIM_TIME = 0.6f;
    public const float LONGRANGE_ANIM_TIME = 1.05f;
    public const float WIDEATTACK_ANIM_TIME = 1.27f;
    public const float MOVE_ANIM_TIME = 1.2f;
    public const float DAMAGE_UPGRADE = 10f;
    public const int BUILD_PRICE = 50;
}