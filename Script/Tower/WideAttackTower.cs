﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WideAttackTower : BaseTower {
    public override void Attack()
    {
        StartCoroutine(MakeSkill(target));
    }

    IEnumerator MakeSkill(BaseEnemy prevTarget)
    {
        if (GameManager.Ins.UM.isDoubleSpeed)
        {
            yield return new WaitForSeconds((Define.WIDEATTACK_ANIM_TIME / 2f) / 2f);
        }
        else
        {
            yield return new WaitForSeconds((Define.WIDEATTACK_ANIM_TIME / 2f));
        }

        // 타겟의 공격이 들어가고 타겟이 범위 넘어가 있는 것을 막기 위해 ( 어색함을 그나마 줄이기 위함 )
        SoundManager.Ins.Play(SoundCategory.WideAttack.ToDesc(), PlayType.OneTime, Channel.Effect);
        BaseEnemy tempTarget = null;
        if (target == null)
            tempTarget = prevTarget;
        else
            tempTarget = target;

        WideAttackParticle particle = GameManager.Ins.PM.GetWideParticle(towerType);
        Vector3 createPos = tempTarget.transform.position;
        createPos.y += 1f;
        particle.transform.position = createPos;
        particle.SetDamage(damage);
        particle.gameObject.SetActive(true);
        
    }

    public override void AttackAnim()
    {
        if (myAnim.GetBool("WideAttack") == false)
        {
            myAnim.SetBool("WideAttack", true);
            StartCoroutine(UnlockAttackAnim());
        }
    }

    public override IEnumerator UnlockAttackAnim()
    {
        yield return new WaitForSeconds(0.1f);
        myAnim.SetBool("WideAttack", false);
    }

    public override void TargetClear()
    {
        UnlockAttackAnim();
        target = null;
    }
}
