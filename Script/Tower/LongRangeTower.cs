﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class LongRangeTower : BaseTower {

    #region INSPECTOR

    public Transform bulletPos;

    #endregion

    public override void Attack()
    {
        StartCoroutine(MakeBullet(target));
    }

    IEnumerator MakeBullet(BaseEnemy prevTarget)
    {
        if(GameManager.Ins.UM.isDoubleSpeed)
        {
            yield return new WaitForSeconds((Define.LONGRANGE_ANIM_TIME/2f) / 2f);
        }
        else
        {
            yield return new WaitForSeconds((Define.LONGRANGE_ANIM_TIME / 2f));
        }

        SoundManager.Ins.Play(SoundCategory.LongRangeAttack.ToDesc(), PlayType.OneTime, Channel.Effect);

        // 타겟의 공격이 들어가고 타겟이 범위 넘어가 있는 것을 막기 위해 ( 어색함을 그나마 줄이기 위함 )
        BaseEnemy tempTarget = null;
        if (target == null)
            tempTarget = prevTarget;
        else
            tempTarget = target;
        BulletParticle particle = GameManager.Ins.PM.GetBulletParticle();
        particle.transform.position = bulletPos.position;
        particle.SetAttackInfo(tempTarget,damage);
        particle.gameObject.SetActive(true);

    }

    public override void AttackAnim()
    {
        if (myAnim.GetBool("LongRangeAttack") == false)
        {
            myAnim.SetBool("LongRangeAttack", true);
            StartCoroutine(UnlockAttackAnim());
        }
    }

    public override IEnumerator UnlockAttackAnim()
    {
        if (GameManager.Ins.UM.isDoubleSpeed)
        {
            yield return new WaitForSeconds(0.1f / 2f);
        }
        else
        {
            yield return new WaitForSeconds(0.1f);
        }
        myAnim.SetBool("LongRangeAttack", false);
    }

    public override void TargetClear()
    {
        UnlockAttackAnim();
        target = null;
    }
}
