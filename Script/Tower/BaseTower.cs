﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class BaseTower : MonoBehaviour {

    virtual public Tower towerName { get { return (Tower)Enum.Parse(typeof(Tower), parsingTowerName); } }

    public float damage;
    public float attackSpeed;
    public float attackDistance;
    public TowerType towerType;
    public State state = State.IDLE;
    public string portraitName;
    public float sellingPrice;


    /* 각 타입별 파티클 */
    public GameObject magicParticle;
    public GameObject rareParticle;
    public GameObject uniqueParticle;
    public GameObject epicParticle;


    // 공격목표
    public BaseEnemy target;

    // 파싱 할 때 필요
    string parsingTowerName;

    public Animator myAnim;

    float attackDelay = 0f;

    void Awake()
    {
        myAnim = GetComponent<Animator>();

        parsingTowerName = name.Substring(0, name.IndexOf("(Clone)"));
        InitInfo();
    }

    public void InitInfo()
    {
        NormalTowerData towerData = towerName.GetNormalTowerValue();
        damage = towerData.damage;
        portraitName = towerData.portraitName;
        attackSpeed = towerData.attackSpeed;
        attackDistance = towerData.attackDistance;
        towerType = towerData.towerType;
        sellingPrice = towerData.sellingPrice;
        state = State.IDLE;

        magicParticle.SetActive(false);
        rareParticle.SetActive(false);
        uniqueParticle.SetActive(false);
        epicParticle.SetActive(false);

        target = null;
        attackDelay = 0f;
    }

    public void SettingDamage(TowerType type)
    {
        switch(type)
        {
            case TowerType.Normal:
                damage = TableManager.Ins.normalTowerTb[towerName].damage;
                break;
            case TowerType.Magic:
                damage = TableManager.Ins.magicTowerTb[towerName].damage;
                break;
            case TowerType.Rare:
                damage = TableManager.Ins.rareTowerTb[towerName].damage;
                break;
            case TowerType.Unique:
                damage = TableManager.Ins.uniqueTowerTb[towerName].damage;
                break;
            case TowerType.Epic:
                damage = TableManager.Ins.epicTowerTb[towerName].damage;
                break;
        }
    }

    public void DoUpdate()
    {
        SearchTarget();
        
        // 타겟 잡자마자 한대 때리고 시작하기 위함
        
        float animSpeed = 1f;
        
        if (GameManager.Ins.UM.isDoubleSpeed)
        {
            attackDelay += (Time.deltaTime * 2f);
            animSpeed *= 2f;
        }
        else
        {
            attackDelay += Time.deltaTime;
        }

        myAnim.speed = animSpeed;

        switch (state)
        {
            case State.ATTACK:
                if (attackDelay >= attackSpeed)
                {
                    AttackAnim();
                    Attack();
                    attackDelay = 0f;
                }
                break;
        }
    }

    private void SearchTarget()
    {
        Vector3 vec_distance;
        float distance;

        // 현재 타겟이 있다면 타겟과 거리 체크
        if (target != null)
        {
            vec_distance = target.transform.position - transform.position;
            distance = (vec_distance.x * vec_distance.x) + (vec_distance.z * vec_distance.z);
            distance = Mathf.Sqrt(distance);

            if (distance <= attackDistance)
            {
                // 타겟 바라보기
                float radian = Mathf.Atan2(vec_distance.z, vec_distance.x);
                float angle = radian * 180 / Mathf.PI;
                transform.rotation = Quaternion.Euler(0, -angle + 90f, 0);


                state = State.ATTACK;
            }
            else
            {
                TargetClear();
                state = State.IDLE;
            }
        }

        // 타겟이 없다면 모든 사용중인 적들과 거리체크하여 타겟 정해주기
        else
        {

            for (int i = GameManager.Ins.SM.usingEnemyList.Count - 1; i >= 0; i--)
            {
                // 거리 구하기
                vec_distance = GameManager.Ins.SM.usingEnemyList[i].transform.position - transform.position;
                distance = (vec_distance.x * vec_distance.x) + (vec_distance.z * vec_distance.z);
                distance = Mathf.Sqrt(distance);

                if (distance <= attackDistance)
                {
                    target = GameManager.Ins.SM.usingEnemyList[i];
                    break;
                }
            }
            state = State.IDLE;
        }
    }


    public virtual void Attack() { }
    public virtual void AttackAnim() { }
    public virtual IEnumerator UnlockAttackAnim() { yield return null; }
    public virtual void TargetClear() { }

    #region Upgrade Function
    public void UpgradeTower()
    {
        switch(towerType)
        {
            case TowerType.Normal:
                UpgradeToMagic();
                break;
            case TowerType.Magic:
                UpgardeToRare();
                break;
            case TowerType.Rare:
                UpgradeToUnique();
                break;
            case TowerType.Unique:
                UpgradeToEpic();
                break;
        }
    }
    private void UpgradeToMagic()
    {
        GameManager.Ins.BM.normalTowerCount[towerName] -= 2;
        if (GameManager.Ins.BM.magicTowerCount.ContainsKey(towerName))
        {
            GameManager.Ins.BM.magicTowerCount[towerName]++;
        }
        else
        {
            GameManager.Ins.BM.magicTowerCount.Add(towerName, 1);
        }

        MagicTowerData towerData = towerName.GetMagicTowerValue();
        damage = towerData.damage;
        attackSpeed = towerData.attackSpeed;
        attackDistance = towerData.attackDistance;
        towerType = towerData.towerType;
        sellingPrice = towerData.sellingPrice;

        StartCoroutine(UpgradeEffect(TowerType.Magic));
    }

    private void UpgardeToRare()
    {
        GameManager.Ins.BM.magicTowerCount[towerName] -= 2;
        if (GameManager.Ins.BM.rareTowerCount.ContainsKey(towerName))
        {
            GameManager.Ins.BM.rareTowerCount[towerName]++;
        }
        else
        {
            GameManager.Ins.BM.rareTowerCount.Add(towerName, 1);
        }

        RareTowerData towerData = towerName.GetRareTowerValue();
        damage = towerData.damage;
        attackSpeed = towerData.attackSpeed;
        attackDistance = towerData.attackDistance;
        towerType = towerData.towerType;
        sellingPrice = towerData.sellingPrice;

        StartCoroutine(UpgradeEffect(TowerType.Rare));
    }

    private void UpgradeToUnique()
    {
        GameManager.Ins.BM.rareTowerCount[towerName] -= 2;
        if (GameManager.Ins.BM.uniqueTowerCount.ContainsKey(towerName))
        {
            GameManager.Ins.BM.uniqueTowerCount[towerName]++;
        }
        else
        {
            GameManager.Ins.BM.uniqueTowerCount.Add(towerName, 1);
        }

        UniqueTowerData towerData = towerName.GetUniqueTowerValue();
        damage = towerData.damage;
        attackSpeed = towerData.attackSpeed;
        attackDistance = towerData.attackDistance;
        towerType = towerData.towerType;
        sellingPrice = towerData.sellingPrice;

        StartCoroutine(UpgradeEffect(TowerType.Unique));
    }

    private void UpgradeToEpic()
    {
        GameManager.Ins.BM.uniqueTowerCount[towerName] -= 2;
        if (GameManager.Ins.BM.epicTowerCount.ContainsKey(towerName))
        {
            GameManager.Ins.BM.epicTowerCount[towerName]++;
        }
        else
        {
            GameManager.Ins.BM.epicTowerCount.Add(towerName, 1);
        }

        EpicTowerData towerData = towerName.GetEpicTowerValue();
        damage = towerData.damage;
        attackSpeed = towerData.attackSpeed;
        attackDistance = towerData.attackDistance;
        towerType = towerData.towerType;
        sellingPrice = towerData.sellingPrice;

        StartCoroutine(UpgradeEffect(TowerType.Epic));
    }

    private IEnumerator UpgradeEffect(TowerType type)
    {
        Vector3 pos;
        switch (type)
        {
            case TowerType.Magic:
                pos = transform.position;
                pos.y += 2f;
                GameManager.Ins.PM.magicUpgrade.transform.position = pos;
                GameManager.Ins.PM.magicUpgrade.SetActive(true);
                break;
            case TowerType.Rare:
                magicParticle.SetActive(false);
                pos = transform.position;
                pos.y += 2f;
                GameManager.Ins.PM.rareUpgrade.transform.position = pos;
                GameManager.Ins.PM.rareUpgrade.SetActive(true);
                break;
            case TowerType.Unique:
                rareParticle.SetActive(false);
                pos = transform.position;
                pos.y += 2f;
                GameManager.Ins.PM.uniqueUpgrade.transform.position = pos;
                GameManager.Ins.PM.uniqueUpgrade.SetActive(true);
                break;
            case TowerType.Epic:
                uniqueParticle.SetActive(false);
                pos = transform.position;
                pos.y += 2f;
                GameManager.Ins.PM.epicUpgrade.transform.position = pos;
                GameManager.Ins.PM.epicUpgrade.SetActive(true);
                break;
        }
        
        
        yield return new WaitForSeconds(0.4f);

        switch (type)
        {
            case TowerType.Magic:
                GameManager.Ins.PM.magicUpgrade.SetActive(false);
                magicParticle.SetActive(true);
                break;
            case TowerType.Rare:
                GameManager.Ins.PM.rareUpgrade.SetActive(false);
                rareParticle.SetActive(true);
                break;
            case TowerType.Unique:
                GameManager.Ins.PM.uniqueUpgrade.SetActive(false);
                uniqueParticle.SetActive(true);
                break;
            case TowerType.Epic:
                GameManager.Ins.PM.epicUpgrade.SetActive(false);
                epicParticle.SetActive(true);
                break;
        }
    }
    #endregion
}
