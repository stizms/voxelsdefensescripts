﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class MeleeTower : BaseTower {
    public override void Attack()
    {
        StartCoroutine(Damage(target));
    }

    IEnumerator Damage(BaseEnemy prevTarget)
    {
        if (GameManager.Ins.UM.isDoubleSpeed)
        {
            yield return new WaitForSeconds(Define.MELEE_ANIM_TIME / 2f);
        }
        else
        {
            yield return new WaitForSeconds(Define.MELEE_ANIM_TIME);
        }

        // 타겟의 공격이 들어가고 타겟이 범위 넘어가 있는 것을 막기 위해 ( 어색함을 그나마 줄이기 위함 )
        SoundManager.Ins.Play(SoundCategory.MeleeAttack.ToDesc(), PlayType.OneTime, Channel.Effect);
        BaseEnemy tempTarget = null;
        if (target == null)
            tempTarget = prevTarget;
        else
            tempTarget = target;

        GameManager.Ins.PM.RequestHitParticle(tempTarget.transform.position);
        tempTarget.Damaged(damage);
    }

    public override void AttackAnim()
    {
        if(myAnim.GetBool("MeleeAttack") == false)
        {
            myAnim.SetBool("MeleeAttack", true);
            StartCoroutine(UnlockAttackAnim());
        }
    }

    public override IEnumerator UnlockAttackAnim()
    {
        yield return new WaitForSeconds(0.1f);
        myAnim.SetBool("MeleeAttack", false);
    }

    public override void TargetClear()
    {
        UnlockAttackAnim();
        target = null;
    }
}
