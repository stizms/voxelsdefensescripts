﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletParticle : MonoBehaviour {

    float damage = 0f;
    BaseEnemy target;
    float moveSpeed = 0f;

    const float SPEED = 5f;

    public void SetAttackInfo(BaseEnemy _target, float _damage)
    {
        damage = _damage;
        target = _target;
    }

    private void Update()
    {
        // 화살이 발사 중에 타겟이 죽는다면
        if (target.gameObject.activeSelf == false)
        {
            ClearBullet();
            return;
        }

        transform.LookAt(target.transform);

        moveSpeed = SPEED;
        if (GameManager.Ins.UM.isDoubleSpeed)
            moveSpeed *= 2f;

        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            if(other.GetComponent<BaseEnemy>() == target)
            {
                target.Damaged(damage);
                ClearBullet();
            }
        }
    }

    private void ClearBullet()
    {
        transform.localPosition = new Vector3(0, 0, 0);
        target = null;
        gameObject.SetActive(false);
    }
}
