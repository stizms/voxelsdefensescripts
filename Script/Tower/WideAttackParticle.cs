﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WideAttackParticle : MonoBehaviour {

    float damage = 0f;
    private BoxCollider myCol;

    List<BaseEnemy> targetList = new List<BaseEnemy>();

    private void Awake()
    {
        myCol = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        StartCoroutine(Damaged());
    }

    IEnumerator Damaged()
    {
        yield return new WaitForSeconds(0.1f);
        myCol.enabled = false;

        for (int i = 0; i < targetList.Count; i++)
        {
            targetList[i].Damaged(damage);
        }

        yield return new WaitForSeconds(0.2f);
        targetList.Clear();
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
        myCol.enabled = true;
        gameObject.SetActive(false);
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.1f);
        myCol.enabled = false;
    }

    public void SetDamage(float _damage)
    {
        damage = _damage;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            targetList.Add(other.GetComponent<BaseEnemy>());
        }
    }
}
