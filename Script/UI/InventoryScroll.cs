﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScroll : MonoBehaviour {
    #region INSPECTOR

    public UIScrollView scrollViewPanel;
    public UIWrapContent wrapContent;
    public List<InventorySlot> slotList;

    #endregion

    private void Start()
    {
        if (wrapContent != null)
        {
            // 세로 일 경우 // 가로에서는 데이터 갯수 - 1 로 해야 함.
            wrapContent.maxIndex = 0;
            wrapContent.onInitializeItem = OnInit; // 델리게이트 (GameObject,int,int)
        }
    }

    public void Refresh()
    {
        int count = (int)Tower.TowerCategoryNumber;

        wrapContent.SortAlphabetically();
        wrapContent.minIndex = -1 * count + 1;

        for (int i = 0; i < slotList.Count; i++)
        {
            if (i < count)
            {
                slotList[i].SetUpData((Tower)i);
            }
            if (i == 0)
                slotList[i].OnSlotClick();
        }

        scrollViewPanel.ResetPosition();
    }

    private void OnInit(GameObject go, int wrapIndex, int dataIndex)
    {
        // 자동으로 넣어짐
        InventorySlot item = go.GetComponent<InventorySlot>();
        if (item != null)
        {
            int index = Mathf.Abs(dataIndex); // 음수 값일 수도 있으므로 ( 세로일 때는 음수 )
            if ((int)Tower.TowerCategoryNumber > index)
            {
                item.SetUpData((Tower)index);
            }
        }
    }
}