﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingScroll : MonoBehaviour {
    #region INSPECTOR

    public UIScrollView scrollViewPanel;
    public UIWrapContent wrapContent;
    public List<RankingSlot> slotList;

    #endregion

    private void Start()
    {
        if (wrapContent != null)
        {
            // 세로 일 경우 // 가로에서는 데이터 갯수 - 1 로 해야 함.
            wrapContent.maxIndex = 0;
            wrapContent.onInitializeItem = OnInit; // 델리게이트 (GameObject,int,int)
        }
    }

    public void Refresh()
    {
        if(GameManager.Ins.UM.isRankingForest)
        {
            int count = TableManager.Ins.forestRankingData.Count;

            wrapContent.SortAlphabetically();
            wrapContent.minIndex = -1 * count + 1;

            for (int i = 0; i < slotList.Count; i++)
            {
                if (i < count)
                {
                    slotList[i].SetUpData(i, TableManager.Ins.forestRankingData[i]);
                }
            }
        }
        else
        {
            int count = TableManager.Ins.iceRankingData.Count;

            wrapContent.SortAlphabetically();
            wrapContent.minIndex = -1 * count + 1;

            for (int i = 0; i < slotList.Count; i++)
            {
                if (i < count)
                {
                    slotList[i].SetUpData(i, TableManager.Ins.iceRankingData[i]);
                }
            }
        }

        scrollViewPanel.ResetPosition();
    }

    public void ClearRefresh()
    {
        if (GameManager.Ins.UM.isRankingForest)
        {
            int count = TableManager.Ins.forestRankingData.Count;

            wrapContent.SortAlphabetically();
            wrapContent.minIndex = -1 * count + 1;

            for (int i = 0; i < slotList.Count; i++)
            {
                if (i < count)
                {
                    slotList[i].ClearData(i);
                }
            }
        }
        else
        {
            int count = TableManager.Ins.iceRankingData.Count;

            wrapContent.SortAlphabetically();
            wrapContent.minIndex = -1 * count + 1;

            for (int i = 0; i < slotList.Count; i++)
            {
                if (i < count)
                {
                    slotList[i].ClearData(i);
                }
            }
        }
    }

    private void OnInit(GameObject go, int wrapIndex, int dataIndex)
    {
        // 자동으로 넣어짐
        RankingSlot ranking = go.GetComponent<RankingSlot>();
        if (ranking != null)
        {
            int index = Mathf.Abs(dataIndex); // 음수 값일 수도 있으므로 ( 세로일 때는 음수 )
            if(GameManager.Ins.UM.isRankingForest)
            {
                if (TableManager.Ins.forestRankingData.Count > index)
                {
                    ranking.SetUpData(index, TableManager.Ins.forestRankingData[index]);
                }
            }
            else
            {
                if (TableManager.Ins.iceRankingData.Count > index)
                {
                    ranking.SetUpData(index, TableManager.Ins.iceRankingData[index]);
                }
            }
        }
    }
}
