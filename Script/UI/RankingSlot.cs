﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingSlot : MonoBehaviour {

    #region INSPECTOR

    public UISprite prizeSprite;
    public UILabel prizeIndex;
    public UILabel userName;
    public UILabel userStage;

    #endregion

    public void SetUpData(int index, RankingData _data)
    {
        prizeIndex.text = "";
        if (index == 0)
        {
            prizeSprite.spriteName = "firstPrize";
        }
        else if (index == 1)
        {
            prizeSprite.spriteName = "secondPrize";
        }
        else if (index == 2)
        {
            prizeSprite.spriteName = "thirdPrize";
        }
        else
        {
            prizeIndex.text = (index+1).ToString();
            prizeSprite.spriteName = "otherPrize";
        }

        userName.text = _data.sID;
        if(GameManager.Ins.UM.isRankingForest)
            userStage.text = _data.forestStage.Substring(5,_data.forestStage.Length-5);
        else
            userStage.text = _data.iceStage.Substring(5, _data.iceStage.Length - 5);
    }

    public void ClearData(int index)
    {
        prizeIndex.text = "";
        if (index == 0)
        {
            prizeSprite.spriteName = "firstPrize";
        }
        else if (index == 1)
        {
            prizeSprite.spriteName = "secondPrize";
        }
        else if (index == 2)
        {
            prizeSprite.spriteName = "thirdPrize";
        }
        else
        {
            prizeIndex.text = index.ToString();
            prizeSprite.spriteName = "otherPrize";
        }

        userName.text = "";
        if (GameManager.Ins.UM.isRankingForest)
            userStage.text = "";
        else
            userStage.text = "";
    }
}
