﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlot : MonoBehaviour {

    #region INSPECTOR

    /* slot 부분 */
    public UILabel nowCardCount;
    public UILabel towerLevel;
    public UILabel towerName;
    public UISprite towerRank;
    public UISprite slotPortrait;

    #endregion

    public Tower towerCategory;
    
	public void SetUpData(Tower _data)
    {
        towerCategory = _data;

        // 타워는 13이 만렙임.
        if (GameManager.Ins.UD.towerData[towerCategory].towerLevel + 1 < 14)
        {
            nowCardCount.text = "( " + GameManager.Ins.UD.towerData[towerCategory].nowCount.ToString() + " / " + (GameManager.Ins.UD.towerData[towerCategory].towerLevel + 1).GetNeedCardValue().needCount.ToString() + " )";
            towerLevel.text = GameManager.Ins.UD.towerData[towerCategory].towerLevel.ToString();
        }
        else
        {
            nowCardCount.text = "MAX";
            towerLevel.text = "13";
        }
        towerName.text = towerCategory.ToDesc();
        towerRank.spriteName = towerCategory.GetNormalTowerValue().rank.ToDesc();
        slotPortrait.spriteName = towerCategory.GetNormalTowerValue().portraitName;
    }


    public void OnSlotClick()
    {
        GameManager.Ins.UM.selectedInventory = this;
        GameManager.Ins.UM.UpdateState(towerCategory);
    }

    public void UpdateSlotData()
    {
        // 타워는 13이 만렙임.
        if (GameManager.Ins.UD.towerData[towerCategory].towerLevel + 1 < 14)
        {
            nowCardCount.text = "( " + GameManager.Ins.UD.towerData[towerCategory].nowCount.ToString() + " / " + (GameManager.Ins.UD.towerData[towerCategory].towerLevel + 1).GetNeedCardValue().needCount.ToString() + " )";
            towerLevel.text = GameManager.Ins.UD.towerData[towerCategory].towerLevel.ToString();
        }
        else
        {
            nowCardCount.text = "MAX";
            towerLevel.text = "13";
        }
    }
}
