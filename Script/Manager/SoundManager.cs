﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
    [Range(0f,1.5f)]
    public float volume = 0.7f;
    [Range(0.5f,1.5f)]
    public float pitch = 1f;

    public void SetClipInfo(AudioSource source)
    {
        source.clip = clip;
        source.pitch = pitch;
    }
}

public class SoundManager : MonoBehaviour {
    #region Singleton

    private static SoundManager instance = null;
    public static SoundManager Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<SoundManager>();
                if (instance == null)
                    instance = new GameObject("SoundManager", typeof(SoundManager)).GetComponent<SoundManager>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    #endregion
    
    // 클립들 등록
    [SerializeField]
    private Sound[] sounds;

    public List<AudioSource> backGroudChannel;
    public List<AudioSource> effectChannel;

    private float backGroundVolumeControl = 1f;
    private float effectVolumeControl = 1f;

    private float originBackGround = 1f;
    private float originEffect = 1f;

    public void Start()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject temp = new GameObject("AudioSource_" + i);
            temp.transform.parent = transform;
            if (i >= 10)
                effectChannel.Add(temp.AddComponent<AudioSource>());
            else
                backGroudChannel.Add(temp.AddComponent<AudioSource>());
        }
    }

    public void Play(string _name,PlayType type, Channel sourceType)
    {
        List<AudioSource> channels = null;

        if (sourceType == Channel.BackGround)
            channels = backGroudChannel;
        else if (sourceType == Channel.Effect)
            channels = effectChannel;

        for (int i = 0; i < channels.Count; i++)
        {
            if (channels[i].isPlaying == false)
            {
                for (int j = 0; j < sounds.Length; j++)
                {
                    if (sounds[j].name == _name)
                    {
                        // 여기서 세팅
                        sounds[j].SetClipInfo(channels[i]);
                        if(sourceType == Channel.BackGround)
                            channels[i].volume = sounds[j].volume * backGroundVolumeControl;
                        else if(sourceType == Channel.Effect)
                            channels[i].volume = sounds[j].volume * effectVolumeControl;
                        if (type == PlayType.Loop)
                            channels[i].loop = true;
                        channels[i].Play();
                        return;
                    }
                }
            }
        }
    }

    public void Stop(string _name,Channel sourceType)
    {
        // 클립 임시로 찾기
        AudioClip temp = null;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                temp = sounds[i].clip;
                break;
            }
        }

        if (temp == null)
            return;

        List<AudioSource> channels = null;

        if (sourceType == Channel.BackGround)
            channels = backGroudChannel;
        else if (sourceType == Channel.Effect)
            channels = effectChannel;

        // 클립을 실행중인 채널 찾고 종료
        for (int i = 0; i < channels.Count; i++)
        {
            if (channels[i].isPlaying)
            {
                if(channels[i].clip == temp)
                {
                    channels[i].Stop();
                    channels[i].loop = false;
                }
            }
        }
    }

    public void ChangeBackgroundVolume(float volume)
    {
        backGroundVolumeControl = volume;

        if(backGroundVolumeControl <= 0f)
        {
            GameManager.Ins.UM.backgroundThumb.spriteName = "slidebar_point_first";
        }
        else
        {
            GameManager.Ins.UM.backgroundThumb.spriteName = "slidebar_point";
        }

        UpdateBackGroundVolume();
    }

    public void ChangeEffectVolume(float volume)
    {
        effectVolumeControl = volume;

        if (effectVolumeControl <= 0f)
        {
            GameManager.Ins.UM.effectThumb.spriteName = "slidebar_point_first";
        }
        else
        {
            GameManager.Ins.UM.effectThumb.spriteName = "slidebar_point";
        }

        UpdateEffectVolume();
    }
    
    public void Mute()
    {
        if(GameManager.Ins.UM.isMute == false)
        {
            originBackGround = backGroundVolumeControl;
            originEffect = effectVolumeControl;

            backGroundVolumeControl = 0f;
            effectVolumeControl = 0f;

            UpdateBackGroundVolume();
            UpdateEffectVolume();

            GameManager.Ins.UM.backgroundThumb.spriteName = "slidebar_point_first";
            GameManager.Ins.UM.effectThumb.spriteName = "slidebar_point_first";

            GameManager.Ins.UM.backgroundSlider.value = 0f;
            GameManager.Ins.UM.effectSlider.value = 0f;
            GameManager.Ins.UM.isMute = true;
        }
        else
        {
            backGroundVolumeControl = originBackGround;
            effectVolumeControl = originEffect;

            UpdateBackGroundVolume();
            UpdateEffectVolume();

            if(backGroundVolumeControl == 0f)
                GameManager.Ins.UM.backgroundThumb.spriteName = "slidebar_point_first";
            else
                GameManager.Ins.UM.backgroundThumb.spriteName = "slidebar_point";

            if (effectVolumeControl == 0f)
                GameManager.Ins.UM.effectThumb.spriteName = "slidebar_point_first";
            else
                GameManager.Ins.UM.effectThumb.spriteName = "slidebar_point";

            GameManager.Ins.UM.backgroundSlider.value = backGroundVolumeControl;
            GameManager.Ins.UM.effectSlider.value = effectVolumeControl;
            GameManager.Ins.UM.isMute = false;
        }
    }


    public void UpdateBackGroundVolume()
    {
        for(int i = 0; i < backGroudChannel.Count; i++)
        {
            float volume = 0f;
            if (backGroudChannel[i].clip != null)
            {
                AudioClip temp = backGroudChannel[i].clip;
                for (int j = 0; j < sounds.Length; j++)
                {
                    if (sounds[j].clip == temp)
                    {
                        volume = sounds[j].volume;
                        break;
                    }
                }
            }
            backGroudChannel[i].volume = volume * backGroundVolumeControl;
        }
    }

    public void UpdateEffectVolume()
    {
        for (int i = 0; i < effectChannel.Count; i++)
        {
            float volume = 0f;
            if (effectChannel[i].clip != null)
            {
                AudioClip temp = effectChannel[i].clip;
                for (int j = 0; j < sounds.Length; j++)
                {
                    if (sounds[j].clip == temp)
                    {
                        volume = sounds[j].volume;
                        break;
                    }
                }
            }
            effectChannel[i].volume = volume * effectVolumeControl;
        }
    }
}

public enum PlayType
{
    OneTime,
    Loop
}

public enum Channel
{
    BackGround,
    Effect,
}

