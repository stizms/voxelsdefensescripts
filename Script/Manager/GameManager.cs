﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    #region Singleton
    private static GameManager instance = null;

    public static GameManager Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                    instance = new GameObject("GameManager", typeof(GameManager)).GetComponent<GameManager>();
            }
            return instance;
        }
    }


    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    #endregion

    private void Start()
    {
        UM = GameObject.Find("UIManager").GetComponent<UIManager>();
        UD = new UserData();

        UM.MainUIInit();

        SoundManager.Ins.Play(SoundCategory.MainBGM.ToDesc(), PlayType.Loop, Channel.BackGround);
     }


   
    public TouchManager TM;
    public TowerManager TowerMng;
    public UIManager UM;
    public BlockManager BM;
    public UserData UD;
    public StageManager SM;
    public ParticleManager PM;
    public AstarManager AM;
}