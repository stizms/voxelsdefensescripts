﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {

    private float startY;
    private float moveY;
    private float zPos;

    private bool isClicked = false;

    private Transform choiceBlock;
    private Ray ray;



    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // 건물관련 UI가 떠 있을때는 터치가 먹지 않게함;
            if (GameManager.Ins.UM.touchControl == true && GameManager.Ins.UM.upgradeChoosing == false)
                return;

            if (Input.mousePosition.x > 200f && Input.mousePosition.x < 1220f && Input.mousePosition.y < 570f && Input.mousePosition.y > 130f)
            {
                isClicked = true;
                startY = Input.mousePosition.y;

                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (hit.collider.CompareTag("BuildMap"))
                    {
                        choiceBlock = hit.transform;
                    }
                }
            }
        }

        // 드래그 한다면
        else if (Input.GetMouseButton(0))
        {
            if (isClicked == false || (GameManager.Ins.UM.touchControl == true && GameManager.Ins.UM.upgradeChoosing == false)) return;

            moveY = Input.mousePosition.y;
            float distance = startY - moveY;

            // 누른채로 일정거리 움직인다면.
            if (distance < -10f || distance > 10f)
            {
                Vector3 cameraPos = Camera.main.transform.position;
                cameraPos.z += distance / 100f;

                if (cameraPos.z > -4.5f)
                {
                    cameraPos.z = -4.5f;
                }
                else if (cameraPos.z < -14.1f)
                {
                    cameraPos.z = -14.1f;
                }

                Camera.main.transform.position = cameraPos;
                startY = moveY;

                choiceBlock = null;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            isClicked = false;

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if(GameManager.Ins.UM.isObstacleBuilding == false)
                {
                    if (hit.collider.CompareTag("BuildMap"))
                    {
                        if (choiceBlock != null && hit.transform == choiceBlock)
                        {
                            if (GameManager.Ins.UM.touchControl == false && GameManager.Ins.UM.upgradeChoosing == false)
                            {
                                // 선택한 타일 저장
                                GameManager.Ins.BM.selectedBlock = hit.transform.GetComponent<BlockInfo>();
                                GameManager.Ins.UM.TowerUIVisible(hit.transform.position);
                            }
                            else if (GameManager.Ins.UM.touchControl == true && GameManager.Ins.UM.upgradeChoosing == true)
                            {
                                if (hit.transform.GetComponent<BlockInfo>().isCreated != false)
                                {
                                    if (GameManager.Ins.BM.targetBlock == GameManager.Ins.BM.selectedBlock)
                                    {
                                        StartCoroutine(GameManager.Ins.UM.FailedLabel("고른 타워와 동일한 타워입니다."));
                                    }
                                    else
                                    {
                                        GameManager.Ins.BM.targetBlock = hit.transform.GetComponent<BlockInfo>();
                                        GameManager.Ins.BM.UpgradeTower();
                                    }
                                }
                                else
                                    StartCoroutine(GameManager.Ins.UM.FailedLabel("타워가 지어지지 않은 땅입니다."));
                            }
                        }
                    }
                }
                else
                {
                    if (hit.collider.CompareTag("ObstacleBuildBlock"))
                    {
                        hit.transform.GetComponent<ObstacleBuildBlock>().CreateObstacle();
                    }
                }
            }
        }

        if(GameManager.Ins.UM.isObstacleBuilding)
        {
            if (Input.GetMouseButtonUp(1))
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (hit.collider.CompareTag("Obstacle"))
                    {
                        for(int i = 0; i < GameManager.Ins.BM.obstacleBlockList.Count; i++)
                        {
                            if(GameManager.Ins.BM.obstacleBlockList[i].obstacle == hit.transform.gameObject)
                            {
                                GameManager.Ins.BM.obstacleBlockList[i].RemoveObstacle();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
