﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UIManager : MonoBehaviour {


    #region INSPECTOR

    public Camera gameUICamera;

    public UI_HPBar hpBarPrefab;
    public Transform hpBarRoot;
    public List<UI_HPBar> hpBarList = new List<UI_HPBar>();

    public GameObject TowerUI;
    public UISprite towerPortrait;
    public UISprite buildSlot;
    public UISprite sellingSlot;
    public UISprite upgradeSlot;


    public GameObject RankingUI;
    public GameObject RankingPanel;
    public RankingScroll rankingScroll;

    public UISprite startSprite;
    public UILabel startLabel;
    
    public UILabel speedLabel;

    public GameObject InventoryUI;
    public GameObject OptionUI;
    public InventoryScroll inventoryScroll;

    public UILabel LifeLabel;
    public UILabel GoldLabel;
    public UILabel CrystalLabel;
    public UILabel playerIDLabel;
    public UILabel nowStageLabel;

    public UILabel gameMessageLabel;


    public GameObject storeUI;
    public StoreScroll storeScroll;

    public InventorySlot selectedInventory;
    // 타워를 팔 때 사용되는 라벨
    public List<UILabel> coinLabelList = new List<UILabel>();


    // stats 부분

    public UISprite statusPortrait;
    public UILabel statusTowerName;
    public UILabel statusTowerLevel;
    public UISprite statusTowerRank;
    public UILabel damageLabel;
    public UILabel attackSpeedLabel;
    public UILabel attackDistanceLabel;
    public UILabel attackTypeLabel;
    public UISprite upgradeBtn;

    public UISprite subRankingForestBtn;
    public UISprite subRankingIceBtn;
    public UILabel subRankingForestLabel;
    public UILabel subRankingIceLabel;

    public UISprite backgroundThumb;
    public UISprite effectThumb;
    public UISlider backgroundSlider;
    public UISlider effectSlider;

    public UILabel rankingSubTitle;

    public UISprite obstacleBuildBtn;

    public GameObject gameOverPopUp;

    public bool touchControl = false;
    public bool upgradeChoosing = false; 
    
    public bool fusionAvailable = false;


    public bool isDoubleSpeed = false;

    public bool isMute = false;

    public bool isForest = true;
    public bool isLabeling = false;
    public bool isRankingForest = true;

    public bool isObstacleBuilding = false;
    #endregion

    #region HP Bar
    private void Start()
    {
        for(int i = 0; i < 50; i++)
        {
            hpBarList.Add(Instantiate(hpBarPrefab, hpBarRoot).GetComponent<UI_HPBar>());
            hpBarList[i].gameObject.SetActive(false);
        }

        if(GameManager.Ins.UD.userLife <= 0)
        {
            startSprite.color = Color.red;
            startLabel.text = "Dead";
            GameManager.Ins.SM.isGaming = false;
        }
    }

    public UI_HPBar RequestHPBar()
    {
        for(int i = 0; i < hpBarList.Count; i++)
        {
            if(hpBarList[i].gameObject.activeSelf == false)
            {
                return hpBarList[i];
            }
        }

        return null;
    }
    #endregion


    
    #region MainUI Function

    public void MainUIInit()
    {
        SetUIUserInfo();
        UpdateLife();
        UpdateCrystal();
        UpdateGold();
    }

    public void UpdateGold()
    {
        int temp = GameManager.Ins.UD.userGold;
        List<int> tempList = new List<int>();
        string goldText = "";

        while(true)
        {
            if(temp < 10)
            {
                tempList.Add(temp);
                break;
            }
            tempList.Add(temp % 10);
            temp /= 10;
        }

        int index = 0;
        int commaIndex = tempList.Count % 3;
        if (commaIndex == 0)
            commaIndex = 3;

        for(int i = tempList.Count - 1 ; i >= 0; i--)
        {
            if(index == commaIndex)
            {
                goldText += ",";
                commaIndex += 3;
            }

            goldText += tempList[i].ToString();
            index++;
        }

        GoldLabel.text = goldText;
    }

    public void UpdateLife()
    {
        LifeLabel.text = GameManager.Ins.UD.userLife.ToString();
    }

    public void UpdateCrystal()
    {
        CrystalLabel.text = GameManager.Ins.UD.userCrystal.ToString();
    }

    public void SetUIUserInfo()
    {
        playerIDLabel.text = GameManager.Ins.UD.userID;
        UpdateStage();
    }

    public void UpdateStage()
    {
        if(GameManager.Ins.UM.isForest)
        {
            nowStageLabel.text = GameManager.Ins.UD.userForestStage.ToString();
            nowStageLabel.color = new Color(1,0.665f,0.033f,1);
            nowStageLabel.effectColor = new Color(0.6981f, 0.3928f, 0.1811f, 1);
        }
        else
        {
            nowStageLabel.text = GameManager.Ins.UD.userIceStage.ToString();
            nowStageLabel.color = new Color(0.1729f, 0, 1, 1);
            nowStageLabel.effectColor = new Color(0.484f, 0.5623f, 0.6981f, 1);
        }
    }

    public void OnStartBtn()
    {
        if (GameManager.Ins.UD.userLife <= 0)
        {
            if(isLabeling == false)
                StartCoroutine(FailedLabel("라이프를 충전해주세요."));
            return;
        }
        else if (GameManager.Ins.SM.isGaming || isObstacleBuilding)
        {
            return;
        }
        else if (GameManager.Ins.SM.isGaming == false && GameManager.Ins.UD.userLife >0)
        {
            startSprite.color = Color.red;
            startLabel.text = "Wait";
            GameManager.Ins.SM.StageStart();
        }
    }

    public void OnDoubleSpeedBtn()
    {
        if(isDoubleSpeed == false)
        {
            speedLabel.text = "x1";
            isDoubleSpeed = true;
        }
        else
        {
            speedLabel.text = "x2";
            isDoubleSpeed = false;
        }
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(),PlayType.OneTime,Channel.Effect);
    }

    public void OnRankingBtn()
    {
        if (TableManager.Ins.isNetworking == true)
            return;

        if (isObstacleBuilding)
            OnObstacleBuildBtn();

        // 아래 함수 있지만 Refresh 때문에 다시한번 초기화
        rankingSubTitle.text = "Forest";
        subRankingForestBtn.spriteName = "shop_tab_s";
        subRankingForestLabel.color = new Color(0.8773f, 0.8048f, 0.4924f, 1);
        subRankingIceBtn.spriteName = "shop_tab_n";
        subRankingIceLabel.color = new Color(0.566f, 0.5527f, 0.4458f, 1);

        isRankingForest = true;

        TowerUI.SetActive(false);
        touchControl = true;
        TableManager.Ins.rankingTb.GetTable().Clear();
        TableManager.Ins.forestRankingData.Clear();
        TableManager.Ins.iceRankingData.Clear();
        RankingUI.SetActive(true);
        var node = StartRankingDownload();
        StartCoroutine(node);
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnRankingForestBtn()
    {
        if (TableManager.Ins.isNetworking)
            return;

        rankingSubTitle.text = "Forest";
        subRankingForestBtn.spriteName = "shop_tab_s";
        subRankingForestLabel.color = new Color(0.8773f,0.8048f,0.4924f,1);
        subRankingIceBtn.spriteName = "shop_tab_n";
        subRankingIceLabel.color = new Color(0.566f,0.5527f,0.4458f,1);

        isRankingForest = true;
        rankingScroll.Refresh();
    }

    public void OnRankingIceBtn()
    {
        if (TableManager.Ins.isNetworking)
            return;

        rankingSubTitle.text = "Ice";
        subRankingForestBtn.spriteName = "shop_tab_n";
        subRankingForestLabel.color = new Color(0.566f, 0.5527f, 0.4458f, 1);
        subRankingIceBtn.spriteName = "shop_tab_s";
        subRankingIceLabel.color = new Color(0.8773f, 0.8048f, 0.4924f, 1);

        isRankingForest = false;
        rankingScroll.Refresh();
    }

    public void OnCloseRankingBtn()
    {
        if(TableManager.Ins.isNetworking)
        {
            StopAllCoroutines();
            TableManager.Ins.isNetworking = false;
            rankingScroll.ClearRefresh();
        }
        RankingPanel.SetActive(false);
        RankingUI.SetActive(false);
        touchControl = false;
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnObstacleBuildBtn()
    {
        if(isObstacleBuilding == false && GameManager.Ins.SM.isGaming == false)
        {
            isObstacleBuilding = true;
            GameManager.Ins.BM.VisibleBuildableBlock();
        }
        else
        {
            isObstacleBuilding = false;
            GameManager.Ins.BM.UnVisibleBuildableBlock();
        }
    }

    IEnumerator StartRankingDownload()
    {
        RankingData data = new RankingData("", GameManager.Ins.UD.userID, "stage" + GameManager.Ins.UD.userForestStage.ToString(),"stage" + GameManager.Ins.UD.userIceStage.ToString());
        TableManager.Ins.isNetworking = true;
        TableManager.Ins.GetTableWWW().PostData(data.GetDBFormID(), typeof(RankingData), data.PostData());
        yield return new WaitForSeconds(1.5f);
        TableManager.Ins.StartRankingDownload();
        yield return new WaitForSeconds(4f);
        TableManager.Ins.isNetworking = false;
        RankingPanel.SetActive(true);
        rankingScroll.Refresh();
    }

    public void OnForestBtn()
    {
        if (isForest == true || GameManager.Ins.SM.isGaming)
            return;

        if (isObstacleBuilding)
            OnObstacleBuildBtn();

        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.SM.forestMap.MapChange();
        isForest = true;
        UpdateStage();
        obstacleBuildBtn.gameObject.SetActive(false);
    }
    public void OnIceBtn()
    {
        if (isForest == false || GameManager.Ins.SM.isGaming)
            return;

        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.SM.iceMap.MapChange();
        isForest = false;
        UpdateStage();
        obstacleBuildBtn.gameObject.SetActive(true);
    }

    public void OnGameOverOkBtn()
    {
        gameOverPopUp.SetActive(false);
        GameManager.Ins.UM.touchControl = false;
    }

    #endregion

    #region Option Btn CallBack

    public void OnOptionBtn()
    {
        if (isObstacleBuilding)
            OnObstacleBuildBtn();

        TowerUI.SetActive(false);
        OptionUI.SetActive(true);
        touchControl = true;
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnOptionCloseBtn()
    {
        OptionUI.SetActive(false);
        touchControl = false;
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnMute()
    {
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        SoundManager.Ins.Mute();
    }
    
    public void OnReset()
    {
        if (GameManager.Ins.SM.isGaming)
            return;

        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.UD.userForestStage = 1;
        GameManager.Ins.UD.userIceStage = 1;
        GameManager.Ins.UD.userLife = 30;
        GameManager.Ins.UD.userGold = 1000;
        MainUIInit();
        

        foreach (var node in GameManager.Ins.BM.obstacleBlockList)
        {
            if(node.obstacle.activeSelf == true)
            {
                node.obstacle.SetActive(false);
                GameManager.Ins.BM.AStarList[node.mapIndex].cost = 1;
            }
        }

        foreach(var node in GameManager.Ins.BM.forestBlockList)
        {
            if(node.isCreated == true)
            {
                node.isCreated = false;
                node.towerInfo.InitInfo();
                node.towerInfo.transform.localPosition = new Vector3(0, 0, 0);
                node.towerInfo.gameObject.SetActive(false);
                node.towerInfo = null;
            }
        }
        foreach (var node in GameManager.Ins.BM.iceBlockList)
        {
            if (node.isCreated == true)
            {
                node.isCreated = false;
                node.towerInfo.InitInfo();
                node.towerInfo.transform.localPosition = new Vector3(0, 0, 0);
                node.towerInfo.gameObject.SetActive(false);
                node.towerInfo = null;
            }
        }
        GameManager.Ins.TowerMng.usingTowerList.Clear();
        GameManager.Ins.BM.normalTowerCount.Clear();
        GameManager.Ins.BM.magicTowerCount.Clear();
        GameManager.Ins.BM.rareTowerCount.Clear();
        GameManager.Ins.BM.uniqueTowerCount.Clear();
        GameManager.Ins.BM.epicTowerCount.Clear();

        GameManager.Ins.UM.startSprite.color = Color.white;
        GameManager.Ins.UM.startLabel.text = "Start";
        GameManager.Ins.SM.isGaming = false;

        GameManager.Ins.UD.SaveData();
    }

    public void ChangeEffectVolume(float volume)
    {
        SoundManager.Ins.ChangeEffectVolume(volume);
    }

    public void ChangeBackgroundVolume(float volume)
    {
        SoundManager.Ins.ChangeBackgroundVolume(volume);
    }

    public void OnSaveAndExit()
    {
        SaveData();

        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Aplication.Quit();
        #endif
    }

    private void SaveData()
    {
        GameManager.Ins.UD.SaveData();
    }

#endregion
    

    #region Inventory Btn CallBack

    public void OnInventoryBtn()
    {
        if (isObstacleBuilding)
            OnObstacleBuildBtn();

        TowerUI.SetActive(false);
        InventoryUI.SetActive(true);
        touchControl = true;
        inventoryScroll.Refresh();
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnInventoryCloseBtn()
    {
        InventoryUI.SetActive(false);
        touchControl = false;
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnUpgradeBtn()
    {
        if (GameManager.Ins.UD.towerData[selectedInventory.towerCategory].nowCount >= (GameManager.Ins.UD.towerData[selectedInventory.towerCategory].towerLevel + 1).GetNeedCardValue().needCount)
        {
            GameManager.Ins.UD.towerData[selectedInventory.towerCategory].nowCount -= (GameManager.Ins.UD.towerData[selectedInventory.towerCategory].towerLevel + 1).GetNeedCardValue().needCount;
            GameManager.Ins.UD.towerData[selectedInventory.towerCategory].towerLevel++;

            // 테이블에 데미지 정보 변경
            TableManager.Ins.UpdateTowerInfo(selectedInventory.towerCategory);
            // 현재 사용중인 타워에도 정보 갱신
            GameManager.Ins.TowerMng.UpdateTowerInfo(selectedInventory.towerCategory);

            UpdateState(selectedInventory.towerCategory);
            selectedInventory.UpdateSlotData();
            GameManager.Ins.UD.SaveData();
            SoundManager.Ins.Play(SoundCategory.UpgradeSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        }
        else
            return;
    }

    public void UpdateState(Tower tower)
    {
        NormalTowerData towerData = tower.GetNormalTowerValue();

        statusPortrait.spriteName = towerData.portraitName;
        statusTowerName.text = towerData.towerName.ToDesc();
        statusTowerLevel.text = GameManager.Ins.UD.towerData[tower].towerLevel.ToString();
        statusTowerRank.spriteName = towerData.rank.ToDesc();
        damageLabel.text = towerData.damage.ToString();
        attackSpeedLabel.text = towerData.attackSpeed.ToString() + " sec";
        attackDistanceLabel.text = towerData.attackDistance.ToString();
        attackTypeLabel.text = towerData.attackType.ToDesc();

        if (GameManager.Ins.UD.towerData[tower].towerLevel + 1 < 14)
        {
            if (GameManager.Ins.UD.towerData[tower].nowCount >= (GameManager.Ins.UD.towerData[tower].towerLevel + 1).GetNeedCardValue().needCount)
                GameManager.Ins.UM.ChangeAlpha(upgradeBtn, 1f);
            else
                GameManager.Ins.UM.ChangeAlpha(upgradeBtn, 0f);
        }
        else
            GameManager.Ins.UM.ChangeAlpha(upgradeBtn, 0f);
    }

    #endregion

    #region Store Btn CallBack

    public void OnStoreBtn()
    {
        if (isObstacleBuilding)
            OnObstacleBuildBtn();

        TowerUI.SetActive(false);
        storeUI.SetActive(true);
        touchControl = true;
        storeScroll.Refresh();
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    public void OnStoreCloseBtn()
    {
        storeUI.SetActive(false);
        touchControl = false;
        SoundManager.Ins.Play(SoundCategory.ExitSound.ToDesc(), PlayType.OneTime, Channel.Effect);
    }

    #endregion

    #region Tower UI Related Func

    public void TowerUIVisible(Vector3 targetPos)
    {
        touchControl = true;
        // UI를 띄워주기 위한 위치 세팅 구간
        Vector3 mainPos = Camera.main.WorldToScreenPoint(targetPos);
        mainPos.z = 0f;
        Vector3 createPos = gameUICamera.ScreenToWorldPoint(mainPos);


        //UI 띄우기
        ChangeAlpha(towerPortrait, 0f);
        TowerUI.SetActive(true);
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        /* 현재 게임 상황을 판단하여 UI 활성 비활성 관련하여 표기 하고 표현하기 */
        if (GameManager.Ins.BM.selectedBlock.isCreated == true)
        {
            ChangeAlpha(towerPortrait, 1f);
            towerPortrait.spriteName = GameManager.Ins.BM.selectedBlock.towerInfo.portraitName;
            ChangeAlpha(buildSlot, 0.5f);
            ChangeAlpha(sellingSlot, 1f);
            
            int count = 0;
            switch(GameManager.Ins.BM.selectedBlock.towerInfo.towerType)
            {
                case TowerType.Normal:
                    count = GameManager.Ins.BM.normalTowerCount[GameManager.Ins.BM.selectedBlock.towerInfo.towerName];
                    break;
                case TowerType.Magic:
                    count = GameManager.Ins.BM.magicTowerCount[GameManager.Ins.BM.selectedBlock.towerInfo.towerName];
                    break;
                case TowerType.Rare:
                    count = GameManager.Ins.BM.rareTowerCount[GameManager.Ins.BM.selectedBlock.towerInfo.towerName];
                    break;
                case TowerType.Unique:
                    count = GameManager.Ins.BM.uniqueTowerCount[GameManager.Ins.BM.selectedBlock.towerInfo.towerName];
                    break;
                case TowerType.Epic:
                    count = GameManager.Ins.BM.epicTowerCount[GameManager.Ins.BM.selectedBlock.towerInfo.towerName];
                    break;
            }
            if(count >= 2)
            {
                fusionAvailable = true;
                ChangeAlpha(upgradeSlot, 1f);
            }
            else
            {
                fusionAvailable = false;
                ChangeAlpha(upgradeSlot, 0.5f);
            }
        }
        else
        {
            ChangeAlpha(buildSlot, 1f);
            ChangeAlpha(sellingSlot, 0.5f);
            ChangeAlpha(upgradeSlot, 0.5f);
        }

        TowerUI.transform.position = createPos;
    }

    public void OnTowerBuildBtn()
    {
        // 이미 지어져 있는데 클릭한다면 작동 안함.
        if(GameManager.Ins.BM.selectedBlock.isCreated == true || GameManager.Ins.UD.userGold < 50)
        {
            return;
        }
        
        GameManager.Ins.UD.userGold -= Define.BUILD_PRICE;
        GameManager.Ins.UM.UpdateGold();
        CoinPooling(GameManager.Ins.BM.selectedBlock.transform, "-", (float)Define.BUILD_PRICE);


        Tower buildIndex = (Tower)Random.Range(0, (int)Tower.TowerCategoryNumber);
        GameManager.Ins.TowerMng.TowerPooling(buildIndex,TowerType.Normal, GameManager.Ins.BM.selectedBlock);
        TowerUI.SetActive(false);
        touchControl = false;
        SoundManager.Ins.Play(SoundCategory.BuildSound.ToDesc(), PlayType.OneTime, Channel.Effect);

        GameManager.Ins.UD.SaveData();
    }

    public void OnTowerSellBtn()
    {
        if(GameManager.Ins.BM.selectedBlock.isCreated == false)
        {
            return;
        }
        SoundManager.Ins.Play(SoundCategory.SellSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        BaseTower target = GameManager.Ins.BM.selectedBlock.towerInfo;
        GameManager.Ins.UD.userGold += (int)target.sellingPrice;

        CoinPooling(target.transform,"+", target.sellingPrice);


        GameManager.Ins.TowerMng.usingTowerList.Remove(target);
        target.transform.localPosition = new Vector3(0, 0, 0);
        target.gameObject.SetActive(false);

        GameManager.Ins.BM.GetCountDictionary(target.towerType)[target.towerName]--;
        GameManager.Ins.BM.selectedBlock.isCreated = false;
        GameManager.Ins.BM.selectedBlock.towerInfo = null;



        GameManager.Ins.UM.UpdateGold();
        TowerUI.SetActive(false);
        touchControl = false;
        
    }

    public void CoinPooling(Transform target,string _str, float money)
    {
        Vector3 originPos = Camera.main.WorldToScreenPoint(target.position);
        originPos.z = 0f;
        originPos.y += 20f;
        Vector3 coinPos = GameManager.Ins.UM.gameUICamera.ScreenToWorldPoint(originPos);

        for (int i = 0; i < coinLabelList.Count; i++)
        {
            if(coinLabelList[i].gameObject.activeSelf == false)
            {
                coinLabelList[i].gameObject.SetActive(true);
                coinLabelList[i].transform.position = coinPos;
                coinLabelList[i].text = _str + " " + money.ToString() + "Gold";

                StartCoroutine(CoinLabelMove(coinLabelList[i]));
                break;
            }
        }
    }

    IEnumerator CoinLabelMove(UILabel coinLabel)
    {
        int delay = 0;
        while(true)
        {
            delay++;
            yield return new WaitForSeconds(0.05f);
            if (delay > 20)
                break;
            Color temp = coinLabel.color;
            temp.a -= 0.05f;
            coinLabel.color = temp;
            coinLabel.transform.localPosition += new Vector3(0, 2.5f, 0);
        }
        coinLabel.gameObject.SetActive(false);
        ChangeAlpha(coinLabel, 1f);
    }
    public void OnTowerFusionBtn()
    {
        if(fusionAvailable == false)
        {
            return;
        }

        TowerUI.SetActive(false);
        gameMessageLabel.gameObject.SetActive(true);
        gameMessageLabel.text = "융합 할 타워를 선택해주세요.";
        upgradeChoosing = true;
    }
    public IEnumerator FailedLabel(string message)
    {
        isLabeling = true;
        gameMessageLabel.gameObject.SetActive(true);
        gameMessageLabel.color = Color.red;
        gameMessageLabel.text = message;
        int delay = 0;
        while (true)
        {
            delay++;
            yield return new WaitForSeconds(0.05f);
            if (delay > 20)
            {
                isLabeling = false;
                break;
            }
            Color temp = gameMessageLabel.color;
            temp.a -= 0.05f;
            gameMessageLabel.color = temp;
        }
        gameMessageLabel.gameObject.SetActive(false);
        gameMessageLabel.color = Color.white;
        ChangeAlpha(gameMessageLabel, 1f);

    }
    public void OnTowerUIExitBtn()
    {
        TowerUI.SetActive(false);
        touchControl = false;
    }
    #endregion

    public void ChangeAlpha<T>(T _target, float _alpha) where T : IColor
    {
        Color temp;
        temp = _target.color;
        temp.a = _alpha;
        _target.color = temp;
    }
}
