﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour {
    public Dictionary<Tower, int> normalTowerCount = new Dictionary<Tower, int>();
    public Dictionary<Tower, int> magicTowerCount = new Dictionary<Tower, int>();
    public Dictionary<Tower, int> rareTowerCount = new Dictionary<Tower, int>();
    public Dictionary<Tower, int> uniqueTowerCount = new Dictionary<Tower, int>();
    public Dictionary<Tower, int> epicTowerCount = new Dictionary<Tower, int>();
    
    public BlockInfo selectedBlock;

    // 업그레이드 타겟 블럭
    public BlockInfo targetBlock;

    // 블럭 리스트
    public List<BlockInfo> forestBlockList = new List<BlockInfo>();
    public List<BlockInfo> iceBlockList = new List<BlockInfo>();

    //경로 변경용 블럭
    public List<ObstacleBuildBlock> obstacleBlockList = new List<ObstacleBuildBlock>();

    // AStar에 사용되는 가중치를 가진 블럭리스트
    public List<Node> AStarList = new List<Node>();

    public Dictionary<Tower, int> GetCountDictionary(TowerType type)
    {
        switch(type)
        {
            case TowerType.Normal:
                return normalTowerCount;
            case TowerType.Magic:
                return magicTowerCount;
            case TowerType.Rare:
                return rareTowerCount;
            case TowerType.Unique:
                return uniqueTowerCount;
            case TowerType.Epic:
                return epicTowerCount;
            default:
                return null;
        }
    }

    public void UpgradeTower()
    {
        if((selectedBlock.towerInfo.towerName == targetBlock.towerInfo.towerName) && (selectedBlock.towerInfo.towerType == targetBlock.towerInfo.towerType))
        {
            SoundManager.Ins.Play(SoundCategory.FusionSound.ToDesc(), PlayType.OneTime, Channel.Effect);
            selectedBlock.towerInfo.UpgradeTower();

            GameManager.Ins.TowerMng.usingTowerList.Remove(targetBlock.towerInfo);
            targetBlock.towerInfo.transform.localPosition = new Vector3(0, 0, 0);
            targetBlock.towerInfo.gameObject.SetActive(false);
            targetBlock.isCreated = false;
            targetBlock.towerInfo = null;
            GameManager.Ins.UM.gameMessageLabel.gameObject.SetActive(false);
        }
        else
        {
            StartCoroutine(GameManager.Ins.UM.FailedLabel("동일한 종류의 타워가 아닙니다."));
        }
        GameManager.Ins.UM.upgradeChoosing = false;
        GameManager.Ins.UM.touchControl = false;
    }

    public void ChangeMap()
    {
        normalTowerCount.Clear();
        magicTowerCount.Clear();
        rareTowerCount.Clear();
        uniqueTowerCount.Clear();
        epicTowerCount.Clear();
        forestBlockList.Clear();
        iceBlockList.Clear();
    }

    public void VisibleBuildableBlock()
    {
        for(int i = 0; i < obstacleBlockList.Count; i++)
        {
            if(obstacleBlockList[i].obstacle.activeSelf == false)
            {
                obstacleBlockList[i].VisibleParticle();
            }
        }
    }

    public void UnVisibleBuildableBlock()
    {
        for (int i = 0; i < obstacleBlockList.Count; i++)
        {
            if (obstacleBlockList[i].obstacle.activeSelf == false)
            {
                obstacleBlockList[i].UnVisibleParticle();
            }
        }
    }
}
