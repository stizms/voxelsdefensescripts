﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {

    #region INSPECTOR

    public Transform particleRoot;

    // upgrade Particle
    public GameObject magicUpgrade;
    public GameObject rareUpgrade;
    public GameObject uniqueUpgrade;
    public GameObject epicUpgrade;

    // wide attack
    public WideAttackParticle normalWideAttackPrefab;
    public List<WideAttackParticle> normalWideList = new List<WideAttackParticle>();
    public WideAttackParticle magicWideAttackPrefab;
    public List<WideAttackParticle> magicWideList = new List<WideAttackParticle>();
    public WideAttackParticle rareWideAttackPrefab;
    public List<WideAttackParticle> rareWideList = new List<WideAttackParticle>();
    public WideAttackParticle uniqueWideAttackPrefab;
    public List<WideAttackParticle> uniqueWideList = new List<WideAttackParticle>();
    public WideAttackParticle epicWideAttackPrefab;
    public List<WideAttackParticle> epicWideList = new List<WideAttackParticle>();

    // LongRange Attack
    public BulletParticle bulletPrefab;
    public List<BulletParticle> bulletList = new List<BulletParticle>();

    // Melee Effect
    public GameObject hitPrefab;
    public List<GameObject> hitParticleList = new List<GameObject>();

    #endregion

    private const int WIDE_PARTICLE_COUNT = 20;

    private void Start()
    {
        for(int i = 0; i < WIDE_PARTICLE_COUNT; i++)
        {
            normalWideList.Add(Instantiate(normalWideAttackPrefab,particleRoot));
            magicWideList.Add(Instantiate(magicWideAttackPrefab, particleRoot));
            rareWideList.Add(Instantiate(rareWideAttackPrefab, particleRoot));
            uniqueWideList.Add(Instantiate(uniqueWideAttackPrefab, particleRoot));
            epicWideList.Add(Instantiate(epicWideAttackPrefab, particleRoot));
            bulletList.Add(Instantiate(bulletPrefab, particleRoot));
            hitParticleList.Add(Instantiate(hitPrefab, particleRoot));

            normalWideList[i].gameObject.SetActive(false);
            magicWideList[i].gameObject.SetActive(false);
            rareWideList[i].gameObject.SetActive(false);
            uniqueWideList[i].gameObject.SetActive(false);
            epicWideList[i].gameObject.SetActive(false);
            bulletList[i].gameObject.SetActive(false);
            hitParticleList[i].gameObject.SetActive(false);
        }
    }

    public WideAttackParticle GetWideParticle(TowerType type)
    {
        List<WideAttackParticle> temp;

        switch(type)
        {
            case TowerType.Normal:
                temp = normalWideList;
                break;
            case TowerType.Magic:
                temp = magicWideList;
                break;
            case TowerType.Rare:
                temp = rareWideList;
                break;
            case TowerType.Unique:
                temp = uniqueWideList;
                break;
            case TowerType.Epic:
                temp = epicWideList;
                break;
            default:
                temp = null;
                return null;
        }
        
        for(int i = 0; i < temp.Count; i++)
        {
            if (temp[i].gameObject.activeSelf == false)
            {
                return temp[i];
            }
            else
                continue;
        }
        return null;
    }

    public BulletParticle GetBulletParticle()
    {
        for (int i = 0; i < bulletList.Count; i++)
        {
            if (bulletList[i].gameObject.activeSelf == false)
            {
                return bulletList[i];
            }
            else
                continue;
        }
        return null;
    }

    public void RequestHitParticle(Vector3 pos)
    {
        for (int i = 0; i < hitParticleList.Count; i++)
        {
            if (hitParticleList[i].gameObject.activeSelf == false)
            {
                StartCoroutine(RequestHit(hitParticleList[i],pos));
            }
            else
                continue;
        }
    }

    IEnumerator RequestHit(GameObject particle , Vector3 pos)
    {
        particle.transform.position = pos;
        particle.SetActive(true);

        yield return new WaitForSeconds(0.3f);

        particle.transform.localPosition = new Vector3(0,0,0);
        particle.SetActive(false);
    }
}
