﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{

    public Transform enemyRoot;

    public MapInfo forestMap;
    public MapInfo iceMap;

    /* 에너미 풀링 리스트 */
    public Dictionary<Enemy, List<BaseEnemy>> enemyList;

    /* 현재 사용중인 에너미 리스트 */
    public List<BaseEnemy> usingEnemyList;

    public List<Transform> pathList;

    private const int POOLING_COUNT = 30;

    public bool isGaming = false;


    public int nowEnemyCount;

    /* 이거 맵이 바뀐다면 받는 방식을 바꿔야 함 */
    public Transform spawnPos;
    public Transform endPos;


    private void Awake()
    {
        enemyList = new Dictionary<Enemy, List<BaseEnemy>>();
        usingEnemyList = new List<BaseEnemy>();
    }

    private void Start()
    {
        for (int i = 0; i < (int)Enemy.EnemyCategoryNumber; i++)
        {
            List<BaseEnemy> tempList = new List<BaseEnemy>();
            for (int j = 0; j < POOLING_COUNT; j++)
            {
                tempList.Add(Instantiate(Resources.Load("Enemy/" + ((Enemy)i).ToDesc()) as GameObject, enemyRoot).GetComponent<BaseEnemy>());
                tempList[j].gameObject.SetActive(false);
            }
            enemyList.Add(((Enemy)i), tempList);
        }

        forestMap.MapChange();
    }

    private void Update()
    {
        // 적 움직이기
        if(usingEnemyList.Count > 0)
        {
            for(int i = 0; i < usingEnemyList.Count; i++)
            {
                usingEnemyList[i].Move();
                usingEnemyList[i].FollowHPBar();
            }
        }
    }

    public void StageStart()
    {
        if (isGaming == true)
            return;
        
        isGaming = true;
        SoundManager.Ins.Play(SoundCategory.StartSound.ToDesc(), PlayType.OneTime, Channel.Effect);

        if(GameManager.Ins.UM.isForest)
            nowEnemyCount = GameManager.Ins.UD.userForestStage.GetStageEnemyValue().count;
        else
            nowEnemyCount = GameManager.Ins.UD.userIceStage.GetStageEnemyValue().count;
        Enemy index = (Enemy)Random.Range(0, (int)Enemy.EnemyCategoryNumber);
        StartCoroutine(SpawnEnemy(nowEnemyCount,index));
    }

    IEnumerator SpawnEnemy(int enemyCount, Enemy index)
    {
        List<BaseEnemy> enemyTemp = enemyList[index];
        for(int i = 0; i < enemyCount; i++)
        {
            if (enemyTemp[i].gameObject.activeSelf == false)
            {
                if (GameManager.Ins.UM.isForest)
                    enemyTemp[i].SetEnemyInfo(GameManager.Ins.UD.userForestStage);
                else
                    enemyTemp[i].SetEnemyInfo(GameManager.Ins.UD.userIceStage);
                enemyTemp[i].transform.position = spawnPos.position;
                enemyTemp[i].gameObject.SetActive(true);
                usingEnemyList.Add(enemyTemp[i]);
            }
            else
                continue;
            if(GameManager.Ins.UM.isDoubleSpeed)
                yield return new WaitForSeconds(0.5f / 2f);
            else
                yield return new WaitForSeconds(0.5f);

        }
    }

    public void CheckGame()
    {
        if (nowEnemyCount <= 0)
        {
            if (GameManager.Ins.UD.userLife > 0)
            {
                if (GameManager.Ins.UM.isForest)
                {
                    if (GameManager.Ins.UD.userForestStage < TableManager.Ins.stageTb.GetTable().Count)
                    {
                        GameManager.Ins.UD.userForestStage++;
                        GameManager.Ins.UM.UpdateStage();
                        GameManager.Ins.UD.SaveData();
                    }
                }
                else
                {
                    if (GameManager.Ins.UD.userIceStage < TableManager.Ins.stageTb.GetTable().Count)
                    {
                        GameManager.Ins.UD.userIceStage++;
                        GameManager.Ins.UM.UpdateStage();
                        GameManager.Ins.UD.SaveData();
                    }
                }
                // 지금은 MAX스테이지 마감처리를 안 해놓음 15스테이지가 끝나도 스테이지는 오르지 않지만 15스테이지로 계속 플레이 가능
                GameManager.Ins.UM.startSprite.color = Color.white;
                GameManager.Ins.UM.startLabel.text = "Start";
                GameManager.Ins.SM.isGaming = false;
            }
        }
        if(GameManager.Ins.UD.userLife <= 0)
        {
            GameManager.Ins.UM.gameOverPopUp.SetActive(true);
            GameManager.Ins.UM.touchControl = true;
            SoundManager.Ins.Play(SoundCategory.GameOverSound.ToDesc(), PlayType.OneTime, Channel.Effect);
            GameManager.Ins.UM.startSprite.color = Color.red;
            GameManager.Ins.UM.startLabel.text = "Dead";
            GameManager.Ins.SM.isGaming = false;
        }
    }
}
