﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour {
    
    public Transform towerRoot;
    

    /* 실질적인 타워 보관 딕셔너리 */
    public Dictionary<Tower, List<BaseTower>> towerList;

    public List<BaseTower> usingTowerList;


    private const int POOLING_COUNT = 10;

    // 링크드 리스트로 해야되나

    private void Awake()
    {
        towerList = new Dictionary<Tower, List<BaseTower>>();
        usingTowerList = new List<BaseTower>();

        for (int i = 0; i < (int)Tower.TowerCategoryNumber; i++)
        {
            List<BaseTower> tempList = new List<BaseTower>();
            for (int j = 0; j < POOLING_COUNT; j++)
            {
                tempList.Add(Instantiate(Resources.Load("Character/" + ((Tower)i).ToDesc()) as GameObject, towerRoot).GetComponent<BaseTower>());
                tempList[j].gameObject.SetActive(false);
            }
            towerList.Add(((Tower)i), tempList);
        }
    }
    public BaseTower TowerPooling(Tower _tower,TowerType _type,BlockInfo _target)
    {
        List<BaseTower> tempList = GameManager.Ins.TowerMng.towerList[_tower];
        for (int i = 0; i < tempList.Count; i++)
        {
            if (tempList[i].gameObject.activeSelf == false)
            {
                GameManager.Ins.TowerMng.usingTowerList.Add(tempList[i]);
                tempList[i].gameObject.SetActive(true);
                tempList[i].transform.position = _target.transform.position;
                tempList[i].towerType = _type;
                _target.isCreated = true;
                _target.towerInfo = tempList[i];
                switch(_type)
                {
                    case TowerType.Normal:
                        if (GameManager.Ins.BM.normalTowerCount.ContainsKey(_tower))
                        {
                            GameManager.Ins.BM.normalTowerCount[_tower]++;
                        }
                        else
                        {
                            GameManager.Ins.BM.normalTowerCount.Add(_tower, 1);
                        }
                        tempList[i].InitInfo();
                        break;
                    case TowerType.Magic:
                        if (GameManager.Ins.BM.magicTowerCount.ContainsKey(_tower))
                        {
                            GameManager.Ins.BM.magicTowerCount[_tower]++;
                        }
                        else
                        {
                            GameManager.Ins.BM.magicTowerCount.Add(_tower, 1);
                        }
                        tempList[i].magicParticle.SetActive(true);
                        break;
                    case TowerType.Rare:
                        if (GameManager.Ins.BM.rareTowerCount.ContainsKey(_tower))
                        {
                            GameManager.Ins.BM.rareTowerCount[_tower]++;
                        }
                        else
                        {
                            GameManager.Ins.BM.rareTowerCount.Add(_tower, 1);
                        }
                        tempList[i].rareParticle.SetActive(true);
                        break;
                    case TowerType.Unique:
                        if (GameManager.Ins.BM.uniqueTowerCount.ContainsKey(_tower))
                        {
                            GameManager.Ins.BM.uniqueTowerCount[_tower]++;
                        }
                        else
                        {
                            GameManager.Ins.BM.uniqueTowerCount.Add(_tower, 1);
                        }
                        tempList[i].uniqueParticle.SetActive(true);
                        break;
                    case TowerType.Epic:
                        if (GameManager.Ins.BM.epicTowerCount.ContainsKey(_tower))
                        {
                            GameManager.Ins.BM.epicTowerCount[_tower]++;
                        }
                        else
                        {
                            GameManager.Ins.BM.epicTowerCount.Add(_tower, 1);
                        }
                        tempList[i].epicParticle.SetActive(true);
                        break;
                }
                return tempList[i];
            }
        }
        return null;
    }

    public void UpdateTowerInfo()
    {
        for (int i = 0; i < usingTowerList.Count; i++)
        {
            switch (usingTowerList[i].towerType)
            {
                case TowerType.Normal:
                    usingTowerList[i].damage = TableManager.Ins.normalTowerTb[usingTowerList[i].towerName].damage;
                    usingTowerList[i].sellingPrice = TableManager.Ins.normalTowerTb[usingTowerList[i].towerName].sellingPrice;
                    break;
                case TowerType.Magic:
                    usingTowerList[i].damage = TableManager.Ins.magicTowerTb[usingTowerList[i].towerName].damage;
                    usingTowerList[i].sellingPrice = TableManager.Ins.magicTowerTb[usingTowerList[i].towerName].sellingPrice;
                    break;
                case TowerType.Rare:
                    usingTowerList[i].damage = TableManager.Ins.rareTowerTb[usingTowerList[i].towerName].damage;
                    usingTowerList[i].sellingPrice = TableManager.Ins.rareTowerTb[usingTowerList[i].towerName].sellingPrice;
                    break;
                case TowerType.Unique:
                    usingTowerList[i].damage = TableManager.Ins.uniqueTowerTb[usingTowerList[i].towerName].damage;
                    usingTowerList[i].sellingPrice = TableManager.Ins.uniqueTowerTb[usingTowerList[i].towerName].sellingPrice;
                    break;
                case TowerType.Epic:
                    usingTowerList[i].damage = TableManager.Ins.epicTowerTb[usingTowerList[i].towerName].damage;
                    usingTowerList[i].sellingPrice = TableManager.Ins.epicTowerTb[usingTowerList[i].towerName].sellingPrice;
                    break;
            }
        }
    }

    public void UpdateTowerInfo(Tower _tower)
    { 
        for (int i = 0; i < usingTowerList.Count; i++)
        {
            if(usingTowerList[i].towerName == _tower)
            {
                switch(usingTowerList[i].towerType)
                {
                    case TowerType.Normal:
                        usingTowerList[i].damage = TableManager.Ins.normalTowerTb[_tower].damage;
                        break;
                    case TowerType.Magic:
                        usingTowerList[i].damage = TableManager.Ins.magicTowerTb[_tower].damage;
                        break;
                    case TowerType.Rare:
                        usingTowerList[i].damage = TableManager.Ins.rareTowerTb[_tower].damage;
                        break;
                    case TowerType.Unique:
                        usingTowerList[i].damage = TableManager.Ins.uniqueTowerTb[_tower].damage;
                        break;
                    case TowerType.Epic:
                        usingTowerList[i].damage = TableManager.Ins.epicTowerTb[_tower].damage;
                        break;
                }
            }
        }
    }

    private void Update()
    {
        if(usingTowerList.Count > 0)
        {
            for(int i = 0; i < usingTowerList.Count; i++)
            {
                usingTowerList[i].DoUpdate();
            }
        }
    }
}
