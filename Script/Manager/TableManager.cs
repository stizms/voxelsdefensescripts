﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour {

    #region Singleton
    private static TableManager instance = null;

    public static TableManager Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<TableManager>();
                if (instance == null)
                    instance = new GameObject("TableManager", typeof(TableManager)).GetComponent<TableManager>();
            }
            return instance;
        }
    }


    private void Awake()
    {
        tableDownLoader = GetComponent<TableDownloader>();
        DontDestroyOnLoad(this);
    }
    #endregion

    private TableDownloader tableDownLoader;
    public ConfigTable configTb { get; } = new ConfigTable();
    public NormalTowerTable normalTowerTb { get; } = new NormalTowerTable();
    public MagicTowerTable magicTowerTb { get; } = new MagicTowerTable();
    public RareTowerTable rareTowerTb { get; } = new RareTowerTable();
    public UniqueTowerTable uniqueTowerTb { get; } = new UniqueTowerTable();
    public EpicTowerTable epicTowerTb { get; } = new EpicTowerTable();
    public TowerUpgradeTable towerUpgradeTb { get; } = new TowerUpgradeTable();
    public StageTable stageTb { get; } = new StageTable();
    public Item_BoxTable item_BoxTb { get; } = new Item_BoxTable();
    public Item_CrystalTable item_CrystalTb { get; } = new Item_CrystalTable();
    public Item_LifeTable item_LifeTb { get; } = new Item_LifeTable();
    public Item_CoinTable item_CoinTb { get; } = new Item_CoinTable();

    public LoginTable loginTb { get; } = new LoginTable();
    public RankingTable rankingTb { get; } = new RankingTable();

    public List<RankingData> forestRankingData = new List<RankingData>();
    public List<RankingData> iceRankingData = new List<RankingData>();

    public bool isNetworking = true;

    public string userIDTemp;

    public TableWWW GetTableWWW()
    {
        return tableDownLoader.GetTableWWW();
    }


    public void StartRankingDownload()
    {
        tableDownLoader.RankingDownload();
    }

    public void StartLoginDownload()
    {
        tableDownLoader.LoginDownload();
    }
    public void StartDownload()
    {
        tableDownLoader.filedGaze.fillAmount = tableDownLoader.fPercent;
        tableDownLoader.Download();
    }

    public ITable GetITable(eTable a_eTable)
    {
        switch (a_eTable)
        {
            case eTable.Config:
                return configTb;
            case eTable.NormalTower:
                return normalTowerTb;
            case eTable.MagicTower:
                return magicTowerTb;
            case eTable.RareTower:
                return rareTowerTb;
            case eTable.UniqueTower:
                return uniqueTowerTb;
            case eTable.EpicTower:
                return epicTowerTb;
            case eTable.TowerUpgrade:
                return towerUpgradeTb;
            case eTable.Stage:
                return stageTb;
            case eTable.Item_Box:
                return item_BoxTb;
            case eTable.Item_Crystal:
                return item_CrystalTb;
            case eTable.Item_Life:
                return item_LifeTb;
            case eTable.Item_Coin:
                return item_CoinTb;
            case eTable.Login:
                return loginTb;
            case eTable.Ranking:
                return rankingTb;
        }

        return null;
    }


    public void UpdateTowerInfo(Tower _tower)
    {
        normalTowerTb[_tower].damage += Define.DAMAGE_UPGRADE;
        magicTowerTb[_tower].damage += Define.DAMAGE_UPGRADE;
        rareTowerTb[_tower].damage += Define.DAMAGE_UPGRADE;
        uniqueTowerTb[_tower].damage += Define.DAMAGE_UPGRADE;
        epicTowerTb[_tower].damage += Define.DAMAGE_UPGRADE;
    }
}
