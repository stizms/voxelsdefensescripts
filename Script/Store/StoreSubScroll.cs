﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreSubScroll : MonoBehaviour {
    #region INSPECTOR

    public UIScrollView scrollViewPanel;
    public UIWrapContent wrapContent;
    public List<ItemSlot> slotList;
    public string tempCategory;
    #endregion
    
    
    public void SetSetting(Store category)
    {
        if (wrapContent != null)
        {
            tempCategory = category.ToDesc();

            switch(category)
            {
                case Store.Box:
                    // 세로 일 경우 // 가로에서는 데이터 갯수 - 1 로 해야 함.
                    wrapContent.maxIndex = (int)Box.BoxCategoryNumber-1;
                    break;
                case Store.Crystal:
                    wrapContent.maxIndex = (int)Crystal.CrystalCategoryNumber - 1;
                    break;
                case Store.Life:
                    wrapContent.maxIndex = (int)Life.LifeCategoryNumber - 1;
                    break;
                case Store.Coin:
                    wrapContent.maxIndex = (int)Coin.CoinCategoryNumber - 1;
                    break;
            }

            wrapContent.onInitializeItem = OnInit; // 델리게이트 (GameObject,int,int)
        }
    }

    public void Refresh()
    {
        int count = (int)Store.ItemCategoryNumber;

        wrapContent.SortAlphabetically();
        wrapContent.minIndex = 0;

        for (int i = 0; i < slotList.Count; i++)
        {
            if (i < count)
            {
                switch(tempCategory)
                {
                    case "Box":
                        slotList[i].SetUpData((Box)i);
                        break;
                    case "Crystal":
                        slotList[i].SetUpData((Crystal)i);
                        break;
                    case "Life":
                        slotList[i].SetUpData((Life)i);
                        break;
                    case "Coin":
                        slotList[i].SetUpData((Coin)i);
                        break;
                }
            }
        }

        scrollViewPanel.ResetPosition();
    }

    private void OnInit(GameObject go, int wrapIndex, int dataIndex)
    {
        // 자동으로 넣어짐
        ItemSlot item = go.GetComponent<ItemSlot>();
        if (item != null)
        {
            int index = Mathf.Abs(dataIndex); // 음수 값일 수도 있으므로 ( 세로일 때는 음수 )
            switch (tempCategory)
            {
                case "Box":
                    if ((int)Box.BoxCategoryNumber > index)
                        item.SetUpData((Box)index);
                    break;
                case "Crystal":
                    if ((int)Crystal.CrystalCategoryNumber > index)
                        item.SetUpData((Crystal)index);
                    break;
                case "Life":
                    if ((int)Life.LifeCategoryNumber > index)
                        item.SetUpData((Life)index);
                    break;
                case "Coin":
                    if ((int)Coin.CoinCategoryNumber > index)
                        item.SetUpData((Coin)index);
                    break;
            }
        }
    }

    public void UpdateGoldAlpha()
    {
        for(int i = 0; i < slotList.Count; i++)
        {
            if (GameManager.Ins.UD.userGold < slotList[i].cost)
                GameManager.Ins.UM.ChangeAlpha(slotList[i].buttonColor, 0.3f);
            else
                GameManager.Ins.UM.ChangeAlpha(slotList[i].buttonColor, 1f);
        }
    }

    public void UpdateCrystalAlpha()
    {
        for (int i = 0; i < slotList.Count; i++)
        {
            if (GameManager.Ins.UD.userCrystal < slotList[i].cost)
                GameManager.Ins.UM.ChangeAlpha(slotList[i].buttonColor, 0.3f);
            else
                GameManager.Ins.UM.ChangeAlpha(slotList[i].buttonColor, 1f);
        }
    }
}