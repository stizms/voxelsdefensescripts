﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_BoxOpen : MonoBehaviour {

    #region INSPECTOR

    public UISprite boxImage;
    public UILabel boxName;

    public TowerInfoSlot dropTower;

    public GameObject BoxInfo;
    public GameObject fadeIn;
    #endregion


    public int min;
    public int max;

    public void SetMinMax(int _min, int _max)
    {
        min = _min;
        max = _max;
    }

    public void CloseBoxOpenUI()
    {
        dropTower.gameObject.SetActive(false);
        BoxInfo.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnClickBox()
    {
        StartCoroutine(BoxOpen());
        BoxInfo.gameObject.SetActive(false);
    }

    IEnumerator BoxOpen()
    {
        fadeIn.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        Tower newTemp = (Tower)Random.Range(min, max);

        NormalTowerData temp = newTemp.GetNormalTowerValue();
        dropTower.towerPortrait.spriteName = temp.portraitName;
        dropTower.towerName.text = temp.towerName.ToDesc();
        dropTower.gameObject.SetActive(true);

        GameManager.Ins.UD.towerData[newTemp].nowCount++;
        fadeIn.SetActive(false);
        SoundManager.Ins.Play(SoundCategory.BoxOpenSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.UD.SaveData();
    }
}
