﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerInfoSlot : MonoBehaviour {
    #region INSPECTOR

    public UISprite towerPortrait;
    public UILabel towerName;

    #endregion
    
    public void OnEnable()
    {
        StartCoroutine(MovePortrait());
    }

    IEnumerator MovePortrait()
    {
        towerPortrait.transform.localPosition = new Vector3(1.7f,4f,-19.6f);
        int delay = 0;
        while (true)
        {
            delay++;
            yield return new WaitForSeconds(0.05f);
            if (delay <= 10)
                towerPortrait.transform.localPosition += new Vector3(0, 0.008f, 0);
            else
                towerPortrait.transform.localPosition -= new Vector3(0, 0.008f, 0);

            if (delay >= 20)
                delay = 0;
        }
    }

    public void OnDisable()
    {
        StopCoroutine(MovePortrait());
    }
}
