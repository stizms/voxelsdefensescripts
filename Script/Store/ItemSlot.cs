﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSlot : MonoBehaviour {

    #region INSPECTOR
    
    // 구입 버튼
    public UISprite buttonColor;
    public UILabel costLabel;
    public UISprite paymentSprite;

    // 아이템 이미지
    public UISprite itemSprite;
    public UILabel itemDescriptionLabel;


    public StoreSubScroll parentScroll;

    public UI_BoxOpen boxOpenUI;
    #endregion

    private string item;
    
    public int cost;
    private int description;

    public void SetUpData(Box index)
    {
        item = index.ToDesc();

        Item_BoxData tempData = index.GetItemBoxValue();
        
        costLabel.text = tempData.itemCost.ToString();
        cost = tempData.itemCost;
        itemSprite.spriteName = tempData.itemName.ToDesc();
        itemDescriptionLabel.text = tempData.itemDescription;
        description = 0;

        if (GameManager.Ins.UD.userGold < cost)
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 0.3f);
        else
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 1f);

        buttonColor.spriteName = "common_btn_color_brown";
        paymentSprite.spriteName = "CoinIcon";
        paymentSprite.color = Color.white;
    }

    public void SetUpData(Crystal index)
    {
        item = index.ToDesc();

        Item_CrystalData tempData = index.GetItemCrystalValue();
       
        costLabel.text = tempData.itemCost.ToString();
        cost = tempData.itemCost;
        itemSprite.spriteName = tempData.itemName.ToDesc();
        itemDescriptionLabel.text = tempData.itemDescription;
        description = int.Parse(tempData.itemDescription);

        buttonColor.spriteName = "common_btn_color_green";
        paymentSprite.spriteName = "PriceIcon";
        paymentSprite.color = Color.yellow;
    }

    public void SetUpData(Life index)
    {
        item = index.ToDesc();

        Item_LifeData tempData = index.GetItemLifeValue();

        costLabel.text = tempData.itemCost.ToString();
        cost = tempData.itemCost;
        itemSprite.spriteName = tempData.itemName.ToDesc();
        itemDescriptionLabel.text = tempData.itemDescription;
        description = int.Parse(tempData.itemDescription);

        if (GameManager.Ins.UD.userGold < cost)
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 0.3f);
        else
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 1f);

        buttonColor.spriteName = "common_btn_color_brown";
        paymentSprite.spriteName = "CoinIcon";
        paymentSprite.color = Color.white;
    }

    public void SetUpData(Coin index)
    {
        item = index.ToDesc();

        Item_CoinData tempData = index.GetItemCoinValue();
                
        costLabel.text = tempData.itemCost.ToString();
        cost = tempData.itemCost;
        itemSprite.spriteName = tempData.itemName.ToDesc();
        itemDescriptionLabel.text = tempData.itemDescription;
        description = int.Parse(tempData.itemDescription);

        if (GameManager.Ins.UD.userCrystal < cost)
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 0.3f);
        else
            GameManager.Ins.UM.ChangeAlpha(buttonColor, 1f);

        buttonColor.spriteName = "common_btn_color_blue";
        paymentSprite.spriteName = "CrystalIcon";
        paymentSprite.color = Color.white;
    }

    public void OnItemBuyClick()
    {
        switch(item)
        {
            case "Box1":
                BuyBox(0, 5, Box.Box1);
                break;
            case "Box2":
                BuyBox(5, 10, Box.Box2);
                break;
            case "Box3":
                BuyBox(10, 18, Box.Box3);
                break;
            case "Box4":
                BuyBox(18,21, Box.Box4);
                break;

            case "Crystal1":
            case "Crystal2":
            case "Crystal3":
            case "Crystal4":
                BuyCrystal();
                break;

            case "Coin1":
            case "Coin2":
            case "Coin3":
            case "Coin4":
                BuyCoin();
                break;

            case "Life1":
            case "Life2":
            case "Life3":
            case "Life4":
                BuyLife();
                break;
        }
    }

    private void BuyBox(int min , int max, Box boxCategory)
    {
        if (GameManager.Ins.UD.userGold < cost)
            return;

        GameManager.Ins.UD.userGold -= cost;
        GameManager.Ins.UM.UpdateGold();

        parentScroll.UpdateGoldAlpha();
        SoundManager.Ins.Play(SoundCategory.BuySound.ToDesc(), PlayType.OneTime, Channel.Effect);
        // 화면 띄우기
        boxOpenUI.boxImage.spriteName = boxCategory.ToDesc();

        // 이부분은 원래 box1이런 이름이 아니라면 필요없었음
        switch(boxCategory)
        {
            case Box.Box1:
                boxOpenUI.boxName.text = "Silver Box";
                break;
            case Box.Box2:
                boxOpenUI.boxName.text = "Gold Box";
                break;
            case Box.Box3:
                boxOpenUI.boxName.text = "Platinum Box";
                break;
            case Box.Box4:
                boxOpenUI.boxName.text = "Diamond Box";
                break;
        }

        boxOpenUI.gameObject.SetActive(true);
        boxOpenUI.SetMinMax(min, max);
    }

    private void BuyLife()
    {
        if (GameManager.Ins.UD.userGold < cost)
            return;
        GameManager.Ins.UD.userGold -= cost;
        GameManager.Ins.UM.UpdateGold();

        if(GameManager.Ins.UD.userLife <= 0)
        {
            GameManager.Ins.UM.startSprite.color = Color.white;
            GameManager.Ins.UM.startLabel.text = "Start";
        }

        GameManager.Ins.UD.userLife += description;
        GameManager.Ins.UM.UpdateLife();
        parentScroll.UpdateGoldAlpha();
        SoundManager.Ins.Play(SoundCategory.BuySound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.UD.SaveData();
    }
    private void BuyCrystal()
    {
        Debug.Log("현금 " + cost.ToString() + @"\ 결제");
        GameManager.Ins.UD.userCrystal += description;
        GameManager.Ins.UM.UpdateCrystal();
        SoundManager.Ins.Play(SoundCategory.BuySound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.UD.SaveData();
    }
    private void BuyCoin()
    {
        if (GameManager.Ins.UD.userCrystal < cost)
            return;
        GameManager.Ins.UD.userCrystal -= cost;
        GameManager.Ins.UM.UpdateCrystal();
        GameManager.Ins.UD.userGold += description;
        GameManager.Ins.UM.UpdateGold();
        parentScroll.UpdateCrystalAlpha();
        SoundManager.Ins.Play(SoundCategory.BuySound.ToDesc(), PlayType.OneTime, Channel.Effect);
        GameManager.Ins.UD.SaveData();

    }
}
