﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCategorySlot : MonoBehaviour {

    #region INSPECTOR

    public UILabel categoryLabel;
    public StoreSubScroll mySubScroll;
    #endregion


    public void SetUpData(Store index)
    {
        categoryLabel.text = index.ToDesc();

        mySubScroll.SetSetting(index);
        mySubScroll.Refresh();
    }
}
