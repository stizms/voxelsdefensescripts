﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInfo : MonoBehaviour {


    #region INSPECTOR

    public Transform startPos;
    public Transform endPos;
    public Map mapTag;
    public List<BlockInfo> buildTileList;
    public List<Transform> pathList = new List<Transform>();

    #endregion

    private void Awake()
    {
        switch(mapTag)
        {
            case Map.Forest:
                GameManager.Ins.BM.forestBlockList = buildTileList;
                GameManager.Ins.SM.pathList = pathList;
                break;
            case Map.Ice:
                GameManager.Ins.BM.iceBlockList = buildTileList;
                break;
        }
    }

    public void MapChange()
    {
        switch (mapTag)
        {
            case Map.Forest:
                Camera.main.transform.position = new Vector3(6.68f, 7.46f, -9.06f);
                break;
            case Map.Ice:
                Camera.main.transform.position = new Vector3(56.68f, 7.46f, -9.06f);
                break;
        }
        GameManager.Ins.SM.spawnPos = startPos;
        GameManager.Ins.SM.endPos = endPos;
    }
}
