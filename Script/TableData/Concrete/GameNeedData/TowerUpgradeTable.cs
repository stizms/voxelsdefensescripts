﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class TowerUpgradeData : TableBase<int>
{
    // TableBase
    public override int key => towerLevel;
    public override eTable eTb => eTable.TowerUpgrade;
    public override string sFileName => "TowerUpgrade.csv";

    // 데이터
    public int towerLevel { get; private set; }
    public int needCount { get; private set; }




    public TowerUpgradeData() { }

    public TowerUpgradeData(string arg0, string arg1)
    {
        SetData(arg0, arg1);
    }

    public TowerUpgradeData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1)
    {

        towerLevel = int.Parse(arg0);
        needCount = int.Parse(arg1);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1]);
    }
}

public class TowerUpgradeTable : Table<int, TowerUpgradeData>
{
}