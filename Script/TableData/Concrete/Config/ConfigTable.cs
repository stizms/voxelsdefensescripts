﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigData : TableBase<string>
{
    // TableBase
    public override string key => configID;
    public override eTable eTb => eTable.Config;
    public override string sFileName => "Config.json";

    // 데이터
    public string configID { get; private set; }
    public string version { get; private set; }

    public ConfigData() { }

    public ConfigData(string s, string v)
    {
        SetData(s, v);
    }

    public ConfigData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string s, string v)
    {
        configID = s;
        version = v;
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1]);
    }
}

public class ConfigTable : Table<string, ConfigData>
{
}