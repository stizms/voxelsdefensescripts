﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class StageData : TableBase<int>
{
    // TableBase
    public override int key => stage;
    public override eTable eTb => eTable.Stage;
    public override string sFileName => "Stage.csv";

    // 데이터
    public int stage { get; set; }
    public int count { get; set; }
    public float hp { get; set; }
    public float speed { get; set; }
    public int money { get; set; }


    public StageData() { }

    public StageData(string arg0, string arg1, string arg2, string arg3, string arg4)
    {
        SetData(arg0, arg1, arg2, arg3, arg4);
    }

    public StageData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4)
    {
        stage = int.Parse(arg0);
        count = int.Parse(arg1);
        hp = float.Parse(arg2);
        speed = float.Parse(arg3);
        money = int.Parse(arg4);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3], s[4]);
    }
}

public class StageTable : Table<int, StageData>
{
}
