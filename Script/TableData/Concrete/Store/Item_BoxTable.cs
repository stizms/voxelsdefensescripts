﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Item_BoxData : TableBase<Box>
{
    // TableBase
    public override Box key => itemName;
    public override eTable eTb => eTable.Item_Box;
    public override string sFileName => "ItemBox.csv";

    // 데이터
    public Box itemName { get; set; }
    public string itemDescription { get; set; }
    public PayCategory itemPay { get; set; }
    public int itemCost { get; set; }



    public Item_BoxData() { }

    public Item_BoxData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public Item_BoxData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {

        itemName = (Box)Enum.Parse(typeof(Box), arg0);
        itemDescription = arg1;
        itemPay = (PayCategory)Enum.Parse(typeof(PayCategory), arg2);
        itemCost = int.Parse(arg3);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3]);
    }
}

public class Item_BoxTable : Table<Box, Item_BoxData>
{
}
