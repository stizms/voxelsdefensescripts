﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class Item_CrystalData : TableBase<Crystal>
{
    // TableBase
    public override Crystal key => itemName;
    public override eTable eTb => eTable.Item_Crystal;
    public override string sFileName => "ItemCrystal.csv";

    // 데이터
    public Crystal itemName { get; set; }
    public string itemDescription { get; set; }
    public PayCategory itemPay { get; set; }
    public int itemCost { get; set; }



    public Item_CrystalData() { }

    public Item_CrystalData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public Item_CrystalData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {

        itemName = (Crystal)Enum.Parse(typeof(Crystal), arg0);
        itemDescription = arg1;
        itemPay = (PayCategory)Enum.Parse(typeof(PayCategory), arg2);
        itemCost = int.Parse(arg3);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3]);
    }
}

public class Item_CrystalTable : Table<Crystal, Item_CrystalData>
{
}