﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class Item_LifeData : TableBase<Life>
{
    // TableBase
    public override Life key => itemName;
    public override eTable eTb => eTable.Item_Life;
    public override string sFileName => "ItemLife.csv";

    // 데이터
    public Life itemName { get; set; }
    public string itemDescription { get; set; }
    public PayCategory itemPay { get; set; }
    public int itemCost { get; set; }



    public Item_LifeData() { }

    public Item_LifeData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public Item_LifeData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {

        itemName = (Life)Enum.Parse(typeof(Life), arg0);
        itemDescription = arg1;
        itemPay = (PayCategory)Enum.Parse(typeof(PayCategory), arg2);
        itemCost = int.Parse(arg3);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3]);
    }
}

public class Item_LifeTable : Table<Life, Item_LifeData>
{
}