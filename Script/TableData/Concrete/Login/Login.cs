﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Login : MonoBehaviour {

    #region INSPECTOR

    public GameObject LoginParent;
    public GameObject LoginPanel;
    public UILabel IDLabel;
    public UILabel PWLabel;
    public GameObject GazePanel;
    public GameObject SignUpPanel;
    public UILabel SignUPIDLabel;
    public UILabel SignUPPWLabel;
    public UILabel LoginMessage;
    public UILabel SignUpMessage;

    #endregion

    private void Start()
    {
        StartCoroutine(StartLoginDownload());
    }

    IEnumerator StartLoginDownload()
    {
        TableManager.Ins.loginTb.GetTable().Clear();
        TableManager.Ins.StartLoginDownload();
        yield return new WaitForSeconds(1.5f);
        TableManager.Ins.isNetworking = false;
        SoundManager.Ins.Play(SoundCategory.LoadingBGM.ToDesc(), PlayType.Loop, Channel.BackGround);
    }

    public void OnLogin()
    {
        if (TableManager.Ins.isNetworking != false)
            return;
        SoundManager.Ins.Play(SoundCategory.PopUpSound.ToDesc(), PlayType.OneTime, Channel.Effect);
        TryLogin();
    }

    public void OnSignUp()
    {
        if (TableManager.Ins.isNetworking != false)
            return;

        IDLabel.text = "";
        PWLabel.text = "";
        LoginMessage.text = "";
        LoginPanel.SetActive(false);
        SignUpPanel.SetActive(true);
    }

    public void OnSignUpToSignUpPanel()
    {
        if (TableManager.Ins.isNetworking != false)
            return;

        TrySignUp();
    }

    public void OnClose()
    {
        if (TableManager.Ins.isNetworking != false)
            return;

        SignUPIDLabel.text = "";
        SignUPPWLabel.text = "";
        SignUpMessage.text = "";
        SignUpPanel.SetActive(false);
        LoginPanel.SetActive(true);
    }
    
    private void TryLogin()
    {
        if (!string.IsNullOrEmpty(IDLabel.text) && !string.IsNullOrEmpty(PWLabel.text))
        {
            LoginMessage.text = "";
            if (TableManager.Ins.loginTb.GetTable().ContainsKey(IDLabel.text))
            {
                if (TableManager.Ins.loginTb[IDLabel.text].password == PWLabel.text)
                {
                    // 로그인
                    TableManager.Ins.userIDTemp = IDLabel.text;
                    LoginParent.SetActive(false);
                    GazePanel.SetActive(true);
                    TableManager.Ins.StartDownload();
                }
                else
                {
                    LoginMessage.text = "ID 또는 Password가 틀렸습니다.";
                }
            }
            else
            {
                LoginMessage.text = "ID 또는 Password가 틀렸습니다.";
            }
        }
        else
        {
            LoginMessage.text = "ID와 PW를 입력해 주십시오.";
        }
    }

    private void TrySignUp()
    {
        if (!string.IsNullOrEmpty(SignUPIDLabel.text) && !string.IsNullOrEmpty(SignUPPWLabel.text))
        {
            SignUpMessage.text = "";
            if (TableManager.Ins.loginTb.GetTable().ContainsKey(SignUPIDLabel.text))
            {
                SignUpMessage.text = "이미 존재하는 ID입니다.";
            }
            else
            {
                LoginData my = new LoginData("", (TableManager.Ins.loginTb.GetTable().Count + 1).ToString(), SignUPIDLabel.text, SignUPPWLabel.text);
                TableManager.Ins.GetTableWWW().PostData(my.GetDBFormID(), typeof(LoginData), my.PostData());
                TableManager.Ins.loginTb.GetTable().Add(my.sID, my);
                SignUpMessage.text = "회원가입에 성공하셨습니다.";
            }
        }
        else
        {
            LoginMessage.text = "ID와 PW를 입력해 주십시오.";
        }
    }
}
