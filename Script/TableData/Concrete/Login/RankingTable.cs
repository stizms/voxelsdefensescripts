﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class RankingData : TableBase<string>
{
    // TableBase
    public override string key => sID;
    public override eTable eTb => eTable.Ranking;
    public override string sFileName => "Ranking.csv";

    public string tStamp { get; set; }
    // 데이터
    [Description("entry.1740767375")]
    public string sID { get; set; }
    [Description("entry.1655010804")]
    public string forestStage { get; set; }
    [Description("entry.70785254")]
    public string iceStage { get; set; }

    public override string GetDBFormID() { return "1FAIpQLSfFc6A6g4YvgspWIK35ZIMBEll7RkpaUXy4Fjex9AMHcMUQyA"; }  // 폼ID

    public RankingData() { }

    public RankingData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public RankingData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2,string arg3)
    {
        tStamp = arg0;
        sID = arg1;
        forestStage = arg2;
        iceStage = arg3;
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3]);
    }

    public override string[] PostData() // 프로퍼티 값중 타임스템프 제외, string배열로 만들기 위한 함수
    {
        List<string> liS = new List<string>();

        liS.Add(sID.ToString());
        liS.Add(forestStage.ToString());
        liS.Add(iceStage.ToString());

        return liS.ToArray();
    }
}

public class RankingTable : Table<string, RankingData>
{
    public override void AddList(List<object> a_refLi)
    {
        Dictionary<string,RankingData> forestRankTb = GetTable();
        Dictionary<string, RankingData> iceRankTb = GetTable();

        foreach (var node in a_refLi)
        {
            RankingData v = (RankingData)node;
            
            if(forestRankTb.ContainsKey(v.sID))
            {
                //// 비교
                if (Convert.ToInt32(forestRankTb[v.sID].forestStage.Substring(5, forestRankTb[v.sID].forestStage.Length - 5)) < Convert.ToInt32(v.forestStage.Substring(5, v.forestStage.Length - 5)))
                {
                    forestRankTb[v.sID].forestStage = v.forestStage;
                }
            }
            else
            {
                forestRankTb.Add(v.sID, v);
            }

            if (iceRankTb.ContainsKey(v.sID))
            {
                //// 비교
                if (Convert.ToInt32(iceRankTb[v.sID].iceStage.Substring(5, iceRankTb[v.sID].iceStage.Length - 5)) < Convert.ToInt32(v.iceStage.Substring(5, v.iceStage.Length - 5)))
                {
                    forestRankTb[v.sID].iceStage = v.iceStage;
                }
            }
            else
            {
                iceRankTb.Add(v.sID, v);
            }
        }

        //// 리스트 화
        foreach (var node in forestRankTb)
        {
            TableManager.Ins.forestRankingData.Add(node.Value);
        }
        foreach (var node in iceRankTb)
        {
            TableManager.Ins.iceRankingData.Add(node.Value);
        }

        // 내림차순
        for (int i = 0; i < TableManager.Ins.forestRankingData.Count - 1; i++)
        {
            for (int j = i + 1; j < TableManager.Ins.forestRankingData.Count; j++)
            {
                if (Convert.ToInt32(TableManager.Ins.forestRankingData[i].forestStage.Substring(5, TableManager.Ins.forestRankingData[i].forestStage.Length-5)) < Convert.ToInt32(TableManager.Ins.forestRankingData[j].forestStage.Substring(5, TableManager.Ins.forestRankingData[j].forestStage.Length - 5)))
                {
                    RankingData temp = TableManager.Ins.forestRankingData[i];
                    TableManager.Ins.forestRankingData[i] = TableManager.Ins.forestRankingData[j];
                    TableManager.Ins.forestRankingData[j] = temp;
                }
            }
        }
        
        for (int i = 0; i < TableManager.Ins.iceRankingData.Count - 1; i++)
        {
            for (int j = i + 1; j < TableManager.Ins.iceRankingData.Count; j++)
            {
                if (Convert.ToInt32(TableManager.Ins.iceRankingData[i].iceStage.Substring(5, TableManager.Ins.iceRankingData[i].iceStage.Length - 5)) < Convert.ToInt32(TableManager.Ins.iceRankingData[j].iceStage.Substring(5, TableManager.Ins.iceRankingData[j].iceStage.Length - 5)))
                {
                    RankingData temp = TableManager.Ins.iceRankingData[i];
                    TableManager.Ins.iceRankingData[i] = TableManager.Ins.iceRankingData[j];
                    TableManager.Ins.iceRankingData[j] = temp;
                }
            }
        }
    }
}