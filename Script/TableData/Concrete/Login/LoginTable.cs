﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.ComponentModel;

public class LoginData : TableBase<string>
{
    // TableBase
    public override string key => sID;
    public override eTable eTb => eTable.Login;
    public override string sFileName => "Login.csv";

    public string tStamp { get; set; }
    // 데이터
    [Description("entry.229048184")]
    public int nID { get; set; }
    [Description("entry.590527682")]
    public string sID { get; set; }
    [Description("entry.1746517762")]
    public string password { get; set; }

    public override string GetDBFormID() { return "1FAIpQLScLMZY3NkrOVSkYCPXhoAtAUDs3hsVsNP_ouND4jRCAdsQCHg"; }  // 폼ID

    public LoginData() { }

    public LoginData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public LoginData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {
        tStamp = arg0;
        nID = int.Parse(arg1);
        sID = arg2;
        password = arg3;
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3]);
    }

    public override string[] PostData() // 프로퍼티 값중 타임스템프 제외, string배열로 만들기 위한 함수
    {
        List<string> liS = new List<string>();

        liS.Add(nID.ToString());
        liS.Add(sID.ToString());
        liS.Add(password.ToString());

        return liS.ToArray();
    }
}

public class LoginTable : Table<string, LoginData>
{
}
