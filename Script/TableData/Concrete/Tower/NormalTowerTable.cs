﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class NormalTowerData : TableBase<Tower>
{
    // TableBase
    public override Tower key => towerName;
    public override eTable eTb => eTable.NormalTower;
    public override string sFileName => "NormalTower.json";

    // 데이터
    public Tower towerName { get; set; }
    public float damage { get; set; }
    public float attackSpeed { get; set; }
    public float attackDistance { get; set; }
    public string portraitName { get; set; }
    public AttackType attackType { get; set; }
    public TowerType towerType { get; set; }
    public float sellingPrice { get; set; }
    public Rank rank { get; set; }




    public NormalTowerData() { }

    public NormalTowerData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    }

    public NormalTowerData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8)
    {

        towerName = (Tower)Enum.Parse(typeof(Tower), arg0);
        damage = float.Parse(arg1);
        attackSpeed = float.Parse(arg2);
        attackDistance = float.Parse(arg3);
        portraitName = arg4;
        attackType = (AttackType)Enum.Parse(typeof(AttackType), arg5);
        towerType = (TowerType)Enum.Parse(typeof(TowerType), arg6);
        sellingPrice = float.Parse(arg7);
        rank = (Rank)Enum.Parse(typeof(Rank), arg8);
    }

    public override void SetData(string[] s)
    {
        SetData(s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8]);
    }
}

public class NormalTowerTable : Table<Tower, NormalTowerData>
{
}
