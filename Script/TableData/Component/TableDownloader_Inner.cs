﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public partial class TableDownloader : MonoBehaviour
{

    public UISprite filedGaze;
    public UILabel LoadingLabel;

    TableWWW m_TbWWW = null;

    int m_nNowDownloadCount = 0;
    int m_nSuccessCount = (int)eTable.Max;
    void AllRequest()
    {
        foreach (var val in m_mapDownloadList)
        {
            ITableIO io = (ITableIO)val.Value.Item2;
            io.Req(val.Key, m_TbWWW,
            (bResult) =>
            {
                val.Value.Item1.AddList(io.liList);
                m_nNowDownloadCount++;
                LoadingLabel.text = "Data Downloading... ( " + m_nNowDownloadCount.ToString() + " / " + m_nSuccessCount.ToString() + " )";

                filedGaze.fillAmount = fPercent;

                if (m_nNowDownloadCount == m_nSuccessCount)
                {
                    StartCoroutine(MoveNextScene());
                }
            });
        }
    }

    void LoginRequest()
    {
        foreach (var val in m_mapDownloadList)
        {
            ITableIO io = (ITableIO)val.Value.Item2;
            io.ReqLogin(val.Key, m_TbWWW,
            (bResult) =>
            {
                val.Value.Item1.AddList(io.liList);
            });
        }
    }

    void RankingRequest()
    {
        foreach (var val in m_mapDownloadList)
        {
            ITableIO io = (ITableIO)val.Value.Item2;
            io.ReqRanking(val.Key, m_TbWWW,
            (bResult) =>
            {
                val.Value.Item1.AddList(io.liList);
            });
        }
    }

    IEnumerator MoveNextScene()
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(GetComponent<Login>());
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        SceneManager.LoadScene("UI_Scene", LoadSceneMode.Additive);
        SoundManager.Ins.Stop(SoundCategory.LoadingBGM.ToDesc(), Channel.BackGround);
    }
    
    void AllFileSave()
    {
        foreach (var val in m_mapDownloadList.Values)
        {
            ITableIO io = (ITableIO)val.Item2;
            io.FileWrite(val.Item3);
        }
    }

    void AllFileRead()
    {
        foreach (var val in m_mapDownloadList.Values)
        {
            ITableIO io = (ITableIO)val.Item2;
            io.FileRead(val.Item3);
        }
    }
}