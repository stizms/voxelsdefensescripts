﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
public partial class TableDownloader : MonoBehaviour
{
    Dictionary<eTable, Tuple<ITable, object, string>> m_mapDownloadList = new Dictionary<eTable, Tuple<ITable, object, string>>();

    public float fPercent { get { return (float)m_nNowDownloadCount / (int)eTable.Max; } }

    public TableWWW GetTableWWW()
    {
        if(m_TbWWW == null)
        {
            m_TbWWW = gameObject.AddComponent<TableWWW>();
        }

        return m_TbWWW;
    }

    public bool RankingDownload()
    {
        if (m_TbWWW == null)
        {
            m_TbWWW = gameObject.AddComponent<TableWWW>();
        }
        if (m_mapDownloadList.Count == 0 ||
            (m_mapDownloadList.Count + 1) != m_nSuccessCount)
        {
            m_mapDownloadList.Clear();

            // SetTables ---------------------------------------------------------------------
            AddTable<string, RankingData>();
            // Download Start ----------------------------------------------------------------
            RankingRequest();
            return true;
        }

        return false;
    }

    public bool LoginDownload()
    {
        if (m_TbWWW == null)
        {
            m_TbWWW = gameObject.AddComponent<TableWWW>();
        }
        if (m_mapDownloadList.Count == 0 ||
            (m_mapDownloadList.Count + 1) != m_nSuccessCount)
        {
            m_mapDownloadList.Clear();

            // SetTables ---------------------------------------------------------------------
            AddTable<string, LoginData>();
            // Download Start ----------------------------------------------------------------
            LoginRequest();
            return true;
        }

        return false;
    }

    public bool Download()
    {
        if (m_TbWWW == null)
        {
            m_TbWWW = gameObject.AddComponent<TableWWW>();
        }
        if (m_mapDownloadList.Count == 0 ||
            (m_mapDownloadList.Count + 1) != m_nSuccessCount)
        {
            m_mapDownloadList.Clear();

            // SetTables ---------------------------------------------------------------------
            AddTable<string, ConfigData>();
            AddTable<Tower, NormalTowerData>();
            AddTable<Tower, MagicTowerData>();
            AddTable<Tower, RareTowerData>();
            AddTable<Tower, UniqueTowerData>();
            AddTable<Tower, EpicTowerData>();
            AddTable<int, TowerUpgradeData>();
            AddTable<int, StageData>();
            AddTable<Box, Item_BoxData>();
            AddTable<Crystal, Item_CrystalData>();
            AddTable<Life, Item_LifeData>();
            AddTable<Coin, Item_CoinData>();
            // Download Start ----------------------------------------------------------------
            AllRequest();
            return true;
        }

        return false;
    }

    void AddTable<K, V>() where V : TableBase<K>, new()
    {
        V v = new V();
        var tb = TableManager.Ins.GetITable(v.eTb);
        object obj = new TableDataIO<V>();
        string s = v.sFileName;

        m_mapDownloadList.Add(v.eTb, Tuple.Create<ITable, object, string>(tb, obj, s));
    }
}
