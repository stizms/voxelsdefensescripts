﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using System;

public class TowerData
{
    public int towerLevel;
    public int nowCount;

    public TowerData(int level, int count)
    {
        towerLevel = level;
        nowCount = count;
    }
}

public class UserData
{
    public string userID;
    public int userForestStage;
    public int userIceStage;
    public int userLife;
    public int userCrystal;
    public int userGold;
    public Dictionary<Tower, TowerData> towerData = new Dictionary<Tower, TowerData>();


    public UserData()
    {
        userID = TableManager.Ins.userIDTemp;
        userForestStage = 1;
        userIceStage = 1;
        userLife = 30;
        userCrystal = 100;
        userGold = 1000;
        foreach (var node in TableManager.Ins.normalTowerTb.GetTable())
        {
            TowerData tempTowerData = new TowerData(1, 1);
            towerData.Add(node.Key, tempTowerData);
        }

        ReadData();

        SettingUserTowerData();

        GameManager.Ins.TowerMng.UpdateTowerInfo();
    }

    private void SettingUserTowerData()
    {
        // 노말 타워 테이블 데미지 변경
        foreach(var node in TableManager.Ins.normalTowerTb.GetTable())
        {
           node.Value.damage += (float)((towerData[node.Key].towerLevel - 1) * 10f);
        }
        // 매직 타워 테이블 데미지 변경
        foreach (var node in TableManager.Ins.magicTowerTb.GetTable())
        {
            node.Value.damage += (float)((towerData[node.Key].towerLevel - 1) * 10f);
        }
        // 레어 타워 테이블 데미지 변경
        foreach (var node in TableManager.Ins.rareTowerTb.GetTable())
        {
            node.Value.damage += (float)((towerData[node.Key].towerLevel - 1) * 10f);
        }
        // 유니크 타워 테이블 데미지 변경
        foreach (var node in TableManager.Ins.uniqueTowerTb.GetTable())
        {
            node.Value.damage += (float)((towerData[node.Key].towerLevel - 1) * 10f);
        }
        // 에픽 타워 테이블 데미지 변경
        foreach (var node in TableManager.Ins.epicTowerTb.GetTable())
        {
            node.Value.damage += (float)((towerData[node.Key].towerLevel - 1) * 10f);
        }
    }

    private void ReadData()
    {
        string path = Application.dataPath + @"\CSVData\";
        string _Filestr = path + userID + "UserData.csv";
        FileInfo fi = new FileInfo(_Filestr);
        if (fi.Exists)
        {
            string[] userDataSplits;
            int index = 0;
            int forestIndex = 0;
            int iceIndex = 0;
            int obstacleIndex = 0;
            using (StreamReader playerDataSR = new StreamReader(_Filestr))
            {
                while (playerDataSR.Peek() >= 0)
                {
                    userDataSplits = playerDataSR.ReadLine().Split(',');

                    if( index == 0)
                    {
                        userID = userDataSplits[0];
                        userForestStage = int.Parse(userDataSplits[1]);
                        userIceStage = int.Parse(userDataSplits[2]);
                        userLife = int.Parse(userDataSplits[3]);
                        userCrystal = int.Parse(userDataSplits[4]);
                        userGold = int.Parse(userDataSplits[5]);
                    }
                    else if ( index >= 1 && index < 23)
                    {
                        Tower tempTower = (Tower)Enum.Parse(typeof(Tower), userDataSplits[0]);
                        towerData[tempTower].towerLevel = int.Parse(userDataSplits[1]);
                        towerData[tempTower].nowCount = int.Parse(userDataSplits[2]);
                    }
                    else if( index >= 23 && index < 30)
                    {
                        bool temp = Convert.ToBoolean(int.Parse(userDataSplits[0]));
                        GameManager.Ins.BM.obstacleBlockList[obstacleIndex].obstacle.SetActive(temp);
                        if(temp)
                            GameManager.Ins.BM.AStarList[GameManager.Ins.BM.obstacleBlockList[obstacleIndex].mapIndex].cost = 200;
                        else
                            GameManager.Ins.BM.AStarList[GameManager.Ins.BM.obstacleBlockList[obstacleIndex].mapIndex].cost = 1;

                        obstacleIndex++;
                    }
                    else if ( index >= 30 && index < 154)
                    {
                        GameManager.Ins.BM.forestBlockList[forestIndex].isCreated = Convert.ToBoolean(int.Parse(userDataSplits[0]));
                        if (GameManager.Ins.BM.forestBlockList[forestIndex].isCreated == true)
                        {
                            GameManager.Ins.BM.forestBlockList[forestIndex].towerInfo = GameManager.Ins.TowerMng.TowerPooling((Tower)Enum.Parse(typeof(Tower), 
                                userDataSplits[2]), (TowerType)Enum.Parse(typeof(TowerType), userDataSplits[1]), 
                                GameManager.Ins.BM.forestBlockList[forestIndex]);
                        }
                        else
                            GameManager.Ins.BM.forestBlockList[forestIndex].towerInfo = null;
                        forestIndex++;
                    }
                    else
                    {
                        GameManager.Ins.BM.iceBlockList[iceIndex].isCreated = Convert.ToBoolean(int.Parse(userDataSplits[0]));
                        if (GameManager.Ins.BM.iceBlockList[iceIndex].isCreated == true)
                        {
                            GameManager.Ins.BM.iceBlockList[iceIndex].towerInfo = GameManager.Ins.TowerMng.TowerPooling((Tower)Enum.Parse(typeof(Tower), userDataSplits[2]), 
                                (TowerType)Enum.Parse(typeof(TowerType), userDataSplits[1]), 
                                GameManager.Ins.BM.iceBlockList[iceIndex]);
                        }
                        else
                            GameManager.Ins.BM.iceBlockList[iceIndex].towerInfo = null;
                        iceIndex++;
                    }
                    index++;
                    System.Array.Clear(userDataSplits, 0, userDataSplits.Length);
                }
            }
        }
        else
        {
            SaveData();
        }
    }

    public void SaveData()
    {
        string path = Application.dataPath + @"\CSVData\";
        string _Filestr = path + userID + "UserData.csv";
        FileInfo fi = new FileInfo(_Filestr);
        if (fi.Exists)
        {
            File.Delete(_Filestr);
        }
        
        using (StreamWriter userDataSW = new StreamWriter(_Filestr, false, System.Text.Encoding.UTF8))
        {
            userDataSW.WriteLine(userID + "," + userForestStage + "," + userIceStage + "," + userLife + "," + userCrystal + "," + userGold + ",");
            foreach (var node in towerData)
            {
                userDataSW.WriteLine(node.Key.ToString() + "," + node.Value.towerLevel + "," + node.Value.nowCount + ",");
            }
            foreach (var node in GameManager.Ins.BM.obstacleBlockList)
            {
                int boolean = 0;
                if (node.obstacle.activeSelf == false)
                {
                    boolean = 0;
                }
                else
                    boolean = 1;
                userDataSW.WriteLine(boolean + ",");
            }
            foreach(var node in GameManager.Ins.BM.forestBlockList)
            {
                int boolean = 0;
                if (node.isCreated == false)
                    boolean = 0;
                else
                    boolean = 1;
                userDataSW.Write(boolean + ",");
                if (node.towerInfo == null)
                    userDataSW.WriteLine(TowerType.None.ToDesc() + "," + Tower.None.ToDesc() + ",");
                else
                    userDataSW.WriteLine(node.towerInfo.towerType.ToDesc() + "," + node.towerInfo.towerName.ToDesc() + ",");
            }
            foreach (var node in GameManager.Ins.BM.iceBlockList)
            {
                int boolean = 0;
                if (node.isCreated == false)
                    boolean = 0;
                else
                    boolean = 1;
                userDataSW.Write(boolean + ",");
                if (node.towerInfo == null)
                    userDataSW.WriteLine(TowerType.None.ToDesc() + "," + Tower.None.ToDesc() + ",");
                else
                    userDataSW.WriteLine(node.towerInfo.towerType.ToDesc() + "," + node.towerInfo.towerName.ToDesc() + ",");
            }
        }
    }
}
