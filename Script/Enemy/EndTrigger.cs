﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if(GameManager.Ins.UD.userLife >0)
        {
            GameManager.Ins.UD.userLife--;
            GameManager.Ins.UM.UpdateLife();

            if(GameManager.Ins.UD.userLife == 0)
                GameManager.Ins.SM.CheckGame();
        }

        other.GetComponent<BaseEnemy>().Dead();
        other.transform.localPosition = new Vector3(0, 0, 0);
        other.gameObject.SetActive(false);
    }
}
