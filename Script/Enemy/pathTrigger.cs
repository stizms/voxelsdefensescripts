﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathTrigger : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            BaseEnemy target = other.GetComponent<BaseEnemy>();
            target.nowPathIndex++;
            target.ChangeTarget();
        }
    }
}
