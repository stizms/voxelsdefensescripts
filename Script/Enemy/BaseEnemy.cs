﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour {

    public float hp;
    public float currentHp;
    public float moveSpeed;
    public int money;

    private float gravity = 20.0f;

    // path트리거에 도달하면 올려줌
    public int nowPathIndex = 0;
    public Transform nowTarget;

    public UI_HPBar myHPBar;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private Animator myAnim;
    
    public List<Transform> pathList = new List<Transform>();

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        myAnim = GetComponent<Animator>();
    }

    public void SetEnemyInfo(int stage)
    {
        StageData tempData = stage.GetStageEnemyValue();
        hp = tempData.hp;
        currentHp = hp;
        moveSpeed = tempData.speed;
        money = tempData.money;
        nowPathIndex = 0;
        myHPBar = GameManager.Ins.UM.RequestHPBar();
        myHPBar.gameObject.SetActive(true);
        if (GameManager.Ins.UM.isForest == true)
            ChangeTarget();
        else
        {
            AStarPathFind();
            nowTarget = pathList[nowPathIndex];
        }

    }

    public void Move()
    {
        float speed;
        float animSpeed = 1f;
        if (controller.isGrounded)
        {
            // 방향 쳐다보기
            Vector3 distance = nowTarget.transform.position - transform.position;
            // 타겟 바라보기
            float radian = Mathf.Atan2(distance.z, distance.x);
            float angle = radian * 180 / Mathf.PI;
            transform.rotation = Quaternion.Euler(0, -angle + 90f, 0);

            if(GameManager.Ins.UM.isForest == false)
            {
                if ((distance.x * distance.x) + (distance.z * distance.z) < (0.15f * 0.15f))
                {
                    nowPathIndex++;
                    nowTarget = pathList[nowPathIndex];
                }
            }

            // nowTarget을 향해 움직이기 ( 속도 x2) 해줘야함
            moveDirection = distance;
            moveDirection.Normalize();

            if (GameManager.Ins.UM.isDoubleSpeed)
            {
                speed = moveSpeed * 2;
                animSpeed *= 1.5f;
            }
            else
            {
                speed = moveSpeed;
            }

            moveDirection *= speed;
            myAnim.speed = animSpeed;
            
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    private void AStarPathFind()
    {
        pathList = GameManager.Ins.AM.pathFinding.FindPath(GameManager.Ins.AM.tileMaker, GameManager.Ins.AM.startPoint, GameManager.Ins.AM.endPoint);
    }

    public void FollowHPBar()
    {
        if( myHPBar!= null)
        {
            myHPBar.gaze.fillAmount = currentHp / hp;

            // UI를 띄워주기 위한 위치 세팅 구간
            Vector3 mainPos = Camera.main.WorldToScreenPoint(transform.position);
            mainPos.z = 0f;
            Vector3 createPos = GameManager.Ins.UM.gameUICamera.ScreenToWorldPoint(mainPos);
            createPos.y += 0.1f;
            myHPBar.transform.position = createPos;
        }
    }

    public void ChangeTarget()
    {
        nowTarget = GameManager.Ins.SM.pathList[nowPathIndex];
    }

    // 공격받음
    public void Damaged(float damage)
    {
        // 중복 체크 방지
        if (currentHp <= 0f)
            return;

        currentHp -= damage;

        if(currentHp <= 0f)
        {
            GameManager.Ins.UM.CoinPooling(transform,"+", money);
            GameManager.Ins.UD.userGold += money;
            GameManager.Ins.UM.UpdateGold();
            Dead();
        }
    }

    public void Dead()
    {
        myHPBar.transform.localPosition = new Vector3(0, 0, 0);
        myHPBar.gameObject.SetActive(false);
        GameManager.Ins.SM.usingEnemyList.Remove(this);
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
        gameObject.SetActive(false);
        GameManager.Ins.SM.nowEnemyCount--;
        if(GameManager.Ins.UD.userLife > 0)
            GameManager.Ins.SM.CheckGame();
    }
}
