﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMaker : Grid {

    const string nameFormat = "x{0:00}_y{1:00}";

    public override Node CreateNode(int cost, int x, int y)
    {   
        Node temp = GameManager.Ins.BM.AStarList[(x * 15) + y];
        temp.Init(cost, x, y);
        temp.name = string.Format(nameFormat, x, y);
        return temp;
    }

    public static int GetX(string s)
    {
        return int.Parse(s.Substring(1, 2));
    }

    public static int GetY(string s)
    {
        return int.Parse(s.Substring(5, 2));
    }

}
