﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBuildBlock : MonoBehaviour {

    #region INSPECTOR

    public GameObject obstacle;
    public GameObject buildableParticle;
    public int mapIndex = 0;
    public int blockIndex = 0;

    #endregion

    public void CreateObstacle()
    {
        if(CheckZeroPath())
        {
            if (GameManager.Ins.UM.isLabeling == false)
            {
                StartCoroutine(GameManager.Ins.UM.FailedLabel("경로가 막혀 장애물을 설치할 수 없습니다."));
            }
            return;
        }
        else
        {
            obstacle.SetActive(true);
            buildableParticle.SetActive(false);
            GameManager.Ins.BM.AStarList[mapIndex].cost = 200;
        }
    }

    public void RemoveObstacle()
    { 
        obstacle.SetActive(false);
        buildableParticle.SetActive(true);
        GameManager.Ins.BM.AStarList[mapIndex].cost = 1;
    }
    
    // 어쩔 수 없이 하드코딩...
    public bool CheckZeroPath()
    {
        // 4가 지어져있는 경우도 생각해야 할듯
        switch(blockIndex)
        {
            case 1:
                if ((GameManager.Ins.BM.obstacleBlockList[6 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[7 - 1].obstacle.activeSelf == true) || 
                    (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[2 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[7 - 1].obstacle.activeSelf == true)) 
                    return true;
                break;
            case 2:
                if ((GameManager.Ins.BM.obstacleBlockList[3 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[5 - 1].obstacle.activeSelf == true) ||
                    (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[1 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[7 - 1].obstacle.activeSelf == true))
                    return true;
                break;
            case 3:
                if ((GameManager.Ins.BM.obstacleBlockList[2 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[5 - 1].obstacle.activeSelf == true) ||
                    (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[5 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[6 - 1].obstacle.activeSelf == true))
                    return true;
                break;
            case 4:
                if ((GameManager.Ins.BM.obstacleBlockList[1 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[2 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[7 - 1].obstacle.activeSelf == true ) 
                    || (GameManager.Ins.BM.obstacleBlockList[3 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[5 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[6 - 1].obstacle.activeSelf == true))
                    return true;
                break;
            case 5:
                if ((GameManager.Ins.BM.obstacleBlockList[2 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[3 - 1].obstacle.activeSelf == true) ||
                    (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[3 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[6 - 1].obstacle.activeSelf == true))
                    return true;
                break;
            case 6:
                if((GameManager.Ins.BM.obstacleBlockList[1 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[7 - 1].obstacle.activeSelf == true) ||
                   (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[5 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[3 - 1].obstacle.activeSelf == true))
                    return true;
                break;
            case 7:
                if ((GameManager.Ins.BM.obstacleBlockList[1 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[6 - 1].obstacle.activeSelf == true) ||
                    (GameManager.Ins.BM.obstacleBlockList[4 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[2 - 1].obstacle.activeSelf == true && GameManager.Ins.BM.obstacleBlockList[1 - 1].obstacle.activeSelf == true))
                    return true;
                break;
        }

        return false;
    }

    public void VisibleParticle()
    {
        buildableParticle.SetActive(true);
    }

    public void UnVisibleParticle()
    {
        buildableParticle.SetActive(false);
    }

}
