﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding {

    public List<Transform> FindPath(Grid grid, Point startPos, Point targetPos)
    {
        List<Transform> pathList = CalcFindPath(grid, startPos, targetPos);
        
        return pathList;
    }

    private List<Transform> CalcFindPath(Grid grid, Point startPos, Point targetPos)
    {
        Node startNode  = grid.nodes[startPos.x, startPos.y];
        Node targetNode = grid.nodes[targetPos.x, targetPos.y];

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();  // 해당 노드 계산 끝났는지 확인용 해쉬셋 ( 유일 값 )
        openSet.Add(startNode);

        // 목적지까지 찾아가는 과정
        while (openSet.Count > 0)
        {
            Node currentNode = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].totalCost < currentNode.totalCost || openSet[i].totalCost == currentNode.totalCost && openSet[i].heuristicCost < currentNode.heuristicCost)
                    currentNode = openSet[i];
            }
            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            // 찾은 경로를 역순으로 보내주기
            if (currentNode == targetNode)
                return ReversePath(grid, startNode, targetNode);

            // 근처에 있는 노드들에 들려서 가중치 계산하기
            foreach (Node neighbour in grid.GetNeighbours(currentNode))
            {
                if (closedSet.Contains(neighbour)) continue;

                int newMovementCostToNeighbour = currentNode.nowCost + GetHeuristic(currentNode, neighbour) * (int)(10.0f * neighbour.cost);
                if (newMovementCostToNeighbour < neighbour.nowCost || !openSet.Contains(neighbour))
                {
                    neighbour.nowCost       = newMovementCostToNeighbour;
                    neighbour.heuristicCost = GetHeuristic(neighbour, targetNode);
                    neighbour.parent        = currentNode;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
        return null;
    }

    private List<Transform> ReversePath(Grid grid, Node startNode, Node endNode)
    {
        List<Transform> path = new List<Transform>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode.transform);
            currentNode = currentNode.parent;
        }
        path.Reverse();
        return path;
    }


    // 휴리스틱 값
    private int GetHeuristic(Node nodeA, Node nodeB)
    {
        int dstX = System.Math.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = System.Math.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        else
            return 14 * dstX + 10 * (dstY - dstX);
    }
}