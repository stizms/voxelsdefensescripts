﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {
    public Node[,] nodes;
    int gridSizeX, gridSizeY;

    public void Init(int width, int height, int[,] tilesMap)
    {
        gridSizeX = width;
        gridSizeY = height;
        nodes = new Node[width, height];

        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y < height; y++)
            {
                nodes[x, y] = CreateNode(tilesMap[x, y], x, y);
            }
        }
    }

    public virtual Node CreateNode(int cost, int x, int y)
    {
        Node node = new Node();
        node.Init(cost, x, y);
        return node;
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        int checkX = 0;
        int checkY = 0;

        // 왼쪽
        checkX = node.gridX + -1;
        checkY = node.gridY + 0;

        if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
        {
            neighbours.Add(nodes[checkX, checkY]);
        }

        checkX = node.gridX + 1;
        checkY = node.gridY + 0;

        if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
        {
            neighbours.Add(nodes[checkX, checkY]);
        }

        // 위
        checkX = node.gridX + 0;
        checkY = node.gridY + 1;

        if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
        {
            neighbours.Add(nodes[checkX, checkY]);
        }

        // 아래
        checkX = node.gridX + 0;
        checkY = node.gridY + -1;

        if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
        {
            neighbours.Add(nodes[checkX, checkY]);
        }

        return neighbours;
    }
}
