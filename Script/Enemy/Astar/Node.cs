﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    
    // x좌표, y좌표
    public int gridX;
    public int gridY;
    public int cost;    // 비용

    [HideInInspector]
    public int nowCost; // 출발부터 여기까지의 비용
    [HideInInspector]
    public int heuristicCost;   // 목적지까지 휴리스틱비용
    [HideInInspector]
    public Node parent;

    public virtual void Init(int cost, int gridX, int gridY)
    {
        this.cost = cost;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public int totalCost
    {
        get
        {
            return nowCost + heuristicCost;
        }
    }
}
